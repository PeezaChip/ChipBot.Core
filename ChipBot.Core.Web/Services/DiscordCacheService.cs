﻿using ChipBot.Core.Shared.Api;
using ChipBot.Core.Web.Models;
using Microsoft.Extensions.Logging;
using System.Net.Http.Json;

namespace ChipBot.Core.Web.Services
{
    public class DiscordCacheService
    {
        private readonly HttpClient httpClient;
        private readonly ILogger<DiscordCacheService> logger;

        private Dictionary<ulong, DiscordUser> users = new();
        private Dictionary<ulong, ApiDiscordRole> roles = new();

        public event Action OnRefresh;

        public DiscordCacheService(HttpClient httpClient, ILogger<DiscordCacheService> logger)
        {
            this.httpClient = httpClient;
            this.logger = logger;

            _ = RefreshUsersAndRoles();
        }

        public DiscordUser GetUser(ulong id)
        {
            if (users == null || !users.Any()) return null;
            var user = users.TryGetValue(id, out var value) ? value : DiscordUser.UnknownUser;
            logger.LogDebug($"Getting user {id} - {user.DisplayName}");
            return user;
        }

        public ApiDiscordRole GetRole(ulong id)
        {
            if (roles == null || !roles.Any()) return null;
            var role = roles.TryGetValue(id, out var value) ? value : ApiDiscordRole.UnknownRole;
            logger.LogDebug($"Getting role {id} - {role.Name}");
            return role;
        }

        private async Task RefreshUsersAndRoles()
        {
            while (true)
            {
                try
                {
                    logger.LogDebug("Refreshing users and roles from api");
                    var usersTask = httpClient.GetFromJsonAsync<List<ApiDiscordUser>>("/discord/users");
                    var rolesTask = httpClient.GetFromJsonAsync<List<ApiDiscordRole>>("/discord/roles");

                    await Task.WhenAll(usersTask, rolesTask);

                    roles = rolesTask.Result.ToDictionary(r => r.Id, r => r);
                    users = usersTask.Result.ToDictionary(u => u.Id, u => new DiscordUser(u, this));

                    logger.LogDebug($"Refreshed {roles.Count} roles and {users.Count} users");

                    OnRefresh?.Invoke();

                    await Task.Delay(TimeSpan.FromMinutes(5));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to refresh users and roles");
                    Console.WriteLine(ex);

                    await Task.Delay(TimeSpan.FromSeconds(30));
                }
            }
        }
    }
}
