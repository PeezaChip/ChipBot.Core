﻿using Microsoft.JSInterop;
using System.Text.Json;

namespace ChipBot.Core.Web.Services
{
    public class LocalStorageService
    {
        private readonly IJSRuntime js;

        public LocalStorageService(IJSRuntime js)
            => this.js = js;

        public async Task<T> GetValueAsync<T>(string key)
        {
            var json = await GetValueAsync(key);
            return json == null ? default : JsonSerializer.Deserialize<T>(json);
        }

        public async Task<string> GetValueAsync(string key)
            => await js.InvokeAsync<string>("localStorage.getItem", key);

        public async Task SetValueAsync<T>(string key, T value)
            => await SetValueAsync(key, JsonSerializer.Serialize(value));

        public async Task SetValueAsync(string key, string value)
            => await js.InvokeVoidAsync("localStorage.setItem", key, value);

        public async Task RemoveAsync(string key)
            => await js.InvokeVoidAsync("localStorage.removeItem", key);

        public async Task Clear()
            => await js.InvokeVoidAsync("localStorage.clear");
    }
}
