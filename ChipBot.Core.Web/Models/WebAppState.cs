﻿namespace ChipBot.Core.Web.Models
{
    public class WebAppState
    {
        private readonly List<LoaderState> loaderStates = new();
        private readonly List<ToastModel> toasts = new();
        public bool IsLoading => loaderStates.Any();

        public LoaderState GetLoader()
        {
            var loader = new LoaderState((l) =>
            {
                loaderStates.Remove(l);
                StateChanged?.Invoke();
            });
            loaderStates.Add(loader);
            StateChanged?.Invoke();
            return loader;
        }

        public void PushToast(ToastModel toast)
        {
            toasts.Add(toast);
            StateChanged?.Invoke();
        }

        public void PopToast(ToastModel toast)
        {
            toasts.Remove(toast);
            StateChanged?.Invoke();
        }

        public IEnumerable<ToastModel> GetToasts()
            => toasts;

        public event Action StateChanged;
    }
}
