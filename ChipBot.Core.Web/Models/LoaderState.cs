﻿namespace ChipBot.Core.Web.Models
{
    public class LoaderState : IDisposable
    {
        private readonly Action<LoaderState> action;

        private bool disposed = false;

        public LoaderState(Action<LoaderState> action)
            => this.action = action;

        public void Dispose()
        {
            if (disposed) return;
            disposed = true;
            action.Invoke(this);
            GC.SuppressFinalize(this);
        }
    }
}
