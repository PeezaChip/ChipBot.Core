﻿namespace ChipBot.Core.Web.Models
{
    public class ToastModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime? DateTime { get; set; }
        public string Icon { get; set; }
        public bool IsSimple { get; set; }
        public bool IsImportant { get; set; }
        public BootstrapColor? Color { get; set; }

        public static ToastModel FromException(Exception ex) =>
            new()
            {
                Icon = "bi bi-exclamation-octagon",
                Message = ex.Message,
                Color = BootstrapColor.Danger,
                IsSimple = true
            };

        public static ToastModel FromWarning(string message) =>
            new()
            {
                Icon = "bi bi-exclamation-square",
                Message = message,
                Color = BootstrapColor.Warning,
                IsSimple = true
            };

        public static ToastModel FromSuccess(string message) =>
            new()
            {
                Icon = "bi bi-check-square",
                Message = message,
                Color = BootstrapColor.Success,
                IsSimple = true
            };
    }
}
