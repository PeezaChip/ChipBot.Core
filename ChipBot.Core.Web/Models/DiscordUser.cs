﻿using ChipBot.Core.Shared.Api;
using ChipBot.Core.Web.Services;

namespace ChipBot.Core.Web.Models
{
    public class DiscordUser : ApiDiscordUser
    {
        public List<ApiDiscordRole> Roles { get; private set; }

        public ApiDiscordRole PrimaryRole => Roles?.OrderByDescending(r => r.Position).FirstOrDefault() ?? ApiDiscordRole.UnknownRole;

        public DiscordUser(ApiDiscordUser user)
        {
            AvatarId = user.AvatarId;
            Discriminator = user.Discriminator;
            DiscriminatorValue = user.DiscriminatorValue;
            IsBot = user.IsBot;
            IsWebhook = user.IsWebhook;
            IsStreaming = user.IsStreaming;
            Username = user.Username;
            Nickname = user.Username;
            DisplayName = user.DisplayName;
            CreatedAt = user.CreatedAt;
            JoinedAt = user.JoinedAt;
            Id = user.Id;
            Mention = user.Mention;
            AvatarUrl = user.AvatarUrl;
            DefaultAvatarUrl = user.DefaultAvatarUrl;
            RoleIds = user.RoleIds;
        }

        public DiscordUser(ApiDiscordUser user, DiscordCacheService service) : this(user)
        {
            Roles = user.RoleIds?
                .Select(service.GetRole)
                .ToList();
        }

        public static new DiscordUser UnknownUser => new(ApiDiscordUser.UnknownUser) { RoleIds = new ulong[] { 0 }, Roles = new List<ApiDiscordRole>() { ApiDiscordRole.UnknownRole } };
    }
}
