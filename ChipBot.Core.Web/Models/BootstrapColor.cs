﻿namespace ChipBot.Core.Web.Models
{
    public enum BootstrapColor
    {
        Primary, PrimarySubtle,
        Secondary, SecondarySubtle,
        Success, SuccessSubtle,
        Danger, DangerSubtle,
        Warning, WarningSubtle,
        Info, InfoSubtle,
        Light, LightSubtle,
        Dark, DarkSubtle,
        Body, BodySecondary, BodyTertiary,
        Black, White,
        Transparent
    }
}
