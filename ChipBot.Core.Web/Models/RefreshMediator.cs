﻿namespace ChipBot.Core.Web.Models
{
    public class RefreshMediator
    {
        public event Action OnRefreshNeeded;

        public void RequireRefresh()
            => OnRefreshNeeded?.Invoke();
    }
}
