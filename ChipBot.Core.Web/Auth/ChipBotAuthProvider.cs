﻿using ChipBot.Core.Shared.Api;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Components.Authorization;
using System.Net.Http.Json;
using System.Security.Claims;

namespace ChipBot.Core.Web.Auth
{
    public class ChipBotAuthProvider : AuthenticationStateProvider
    {
        private readonly HttpClient httpClient;

        public ChipBotAuthProvider(HttpClient httpClient) => this.httpClient = httpClient;

        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            try
            {
                var userInfo = await httpClient.GetFromJsonAsync<UserInfo>("/auth/userinfo");
                if (userInfo != null)
                {
                    return GetAuthenticationStateFromUserInfo(userInfo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return Unauthorized;
        }

        private static AuthenticationState GetAuthenticationStateFromUserInfo(UserInfo userInfo)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, userInfo.Username),
                new(ClaimTypes.NameIdentifier, userInfo.UserId.ToString(), ClaimValueTypes.UInteger64),
                new(ClaimDefaults.Avatar, userInfo.Avatar)
            };

            claims.AddRange(userInfo.Roles.Select(r => new Claim(ClaimTypes.Role, r)));

            return new(new ClaimsPrincipal(new ClaimsIdentity(claims, AuthDefaults.Scheme)));
        }

        private static readonly AuthenticationState Unauthorized = new(new ClaimsPrincipal(new ClaimsIdentity()));
    }
}
