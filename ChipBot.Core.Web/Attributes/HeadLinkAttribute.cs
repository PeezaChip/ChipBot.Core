﻿namespace ChipBot.Core.Web.Attributes
{
    public class HeadLinkAttribute : HeadItemBaseAttribute
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public bool AppendVersion { get; set; } = false;
    }
}
