﻿namespace ChipBot.Core.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public abstract class HeadItemBaseAttribute : Attribute { }
}
