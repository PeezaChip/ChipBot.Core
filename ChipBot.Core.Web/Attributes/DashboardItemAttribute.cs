﻿namespace ChipBot.Core.Web.Attributes
{
    public class DashboardItemAttribute : NavItemBaseAttribute
    {
        public Type ComponentType { get; set; }
    }
}
