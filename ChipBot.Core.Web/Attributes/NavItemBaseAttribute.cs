﻿namespace ChipBot.Core.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public abstract class NavItemBaseAttribute : Attribute
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Roles { get; set; }
        public int Order { get; set; } = 10;
    }
}
