﻿namespace ChipBot.Core.Web.Attributes
{
    public class UserSettingsItemAttribute : NavItemBaseAttribute
    {
        public Type ComponentType { get; set; }
    }
}
