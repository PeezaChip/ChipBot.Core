﻿using Microsoft.AspNetCore.Components.Routing;

namespace ChipBot.Core.Web.Attributes
{
    public class PageInfoAttribute : NavItemBaseAttribute
    {
        public string Category { get; set; }
        public string Link { get; set; }
        public NavLinkMatch NavLinkMatch { get; set; } = NavLinkMatch.Prefix;
        public bool Hidden { get; set; }
    }
}
