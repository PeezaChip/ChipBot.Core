﻿using ChipBot.Core.Web.Abstract;
using ChipBot.Core.Web.Attributes;
using ChipBot.Core.Web.Auth;
using ChipBot.Core.Web.Models;
using ChipBot.Core.Web.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace ChipBot.Core.Web.Extensions
{
    public static class ChipBotWebExtensions
    {
        public static IServiceCollection AddChipBotWebCore(this IServiceCollection services, List<Assembly> assemblies, Type layout)
        {
            assemblies.Add(Assembly.GetExecutingAssembly());

            services
                .AddAuthorizationCore()
                .AddScoped<AuthenticationStateProvider, ChipBotAuthProvider>();

            services.AddSingleton(new LayoutPackage() { Type = layout });

            services.AddSingleton(assemblies);

            var exportedTypes = assemblies.SelectMany(a => a.ExportedTypes);
            services.AddSingleton(GetNavItems(exportedTypes));
            services.AddSingleton(GetDashboardItems(exportedTypes));
            services.AddSingleton(GetUserSettingsItems(exportedTypes));
            services.AddSingleton(GetHeadItems(exportedTypes));

            services.AddScoped<DiscordCacheService>();

            services.AddSingleton<LocalStorageService>();

            return services;
        }

        private static IOrderedEnumerable<DashboardItemAttribute> GetDashboardItems(IEnumerable<Type> types)
        {
            var dashboardAttributes = types
                .Where(t => t.GetCustomAttribute<DashboardItemAttribute>() != null)
                .Select(t => t.GetCustomAttribute<DashboardItemAttribute>());

            return dashboardAttributes.OrderBy(i => i.Order);
        }

        private static IOrderedEnumerable<UserSettingsItemAttribute> GetUserSettingsItems(IEnumerable<Type> types)
        {
            var userSettingsItemAttributes = types
                .Where(t => t.GetCustomAttribute<UserSettingsItemAttribute>() != null)
                .Select(t => t.GetCustomAttribute<UserSettingsItemAttribute>());

            return userSettingsItemAttributes.OrderBy(i => i.Order);
        }

        private static IOrderedEnumerable<NavItem> GetNavItems(IEnumerable<Type> types)
        {
            var pageInfoAttributes = types
                .Where(t => t.GetCustomAttribute<PageInfoAttribute>() != null)
                .Select(t => t.GetCustomAttribute<PageInfoAttribute>());

            var pageCategoryAttributes = types
                .Where(t => t.GetCustomAttribute<PageCategoryAttribute>() != null)
                .Select(t => t.GetCustomAttribute<PageCategoryAttribute>())
                .DistinctBy(c => c.Name);

            var result = new List<NavItem>();

            var categories = new Dictionary<string, PageCategory>();
            foreach (var pageCategory in pageCategoryAttributes)
            {
                var category = new PageCategory()
                {
                    Icon = pageCategory.Icon,
                    Name = pageCategory.Name,
                    Order = pageCategory.Order,
                    Roles = pageCategory.Roles,
                    Items = new List<PageInfo>()
                };
                categories.Add(pageCategory.Name, category);
                result.Add(category);
            }

            foreach (var pageInfo in pageInfoAttributes)
            {
                var page = new PageInfo()
                {
                    Hidden = pageInfo.Hidden,
                    Icon = pageInfo.Icon,
                    Name = pageInfo.Name,
                    Order = pageInfo.Order,
                    Link = pageInfo.Link,
                    NavLinkMatch = pageInfo.NavLinkMatch,
                    Roles = pageInfo.Roles
                };
                if (!string.IsNullOrEmpty(pageInfo.Category) && categories.TryGetValue(pageInfo.Category, out var category))
                {
                    page.Category = category;
                    category.Items.Add(page);
                }
                else
                {
                    result.Add(page);
                }
            }

            return result.OrderBy(i => i.Order);
        }

        private static IEnumerable<HeadItem> GetHeadItems(IEnumerable<Type> types)
        {
            var result = new List<HeadItem>();

            var headLinkAttributes = types
                .Where(t => t.GetCustomAttribute<HeadLinkAttribute>() != null)
                .Select(t => t.GetCustomAttribute<HeadLinkAttribute>())
                .DistinctBy(a => $"{a.Rel}+{a.Href}");

            var version = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

            foreach (var attribute in headLinkAttributes)
            {
                result.Add(new HeadLinkItem() { Href = attribute.AppendVersion ? $"{attribute.Href}?v={version}" : attribute.Href, Rel = attribute.Rel });
            }

            return result;
        }
    }
}
