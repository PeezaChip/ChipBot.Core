﻿using System.Text.RegularExpressions;

namespace ChipBot.Core.Web.Extensions
{
    public static partial class EnumExtensions
    {
        public static string ToKebabCase(this Enum value)
        {
            return KebabCaseReplace().Replace(value.ToString(), "-$1")
                .Trim()
                .ToLower();
        }

        [GeneratedRegex("(?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z0-9])", RegexOptions.Compiled)]
        private static partial Regex KebabCaseReplace();
    }
}
