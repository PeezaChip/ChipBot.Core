﻿namespace ChipBot.Core.Web.Extensions
{
    public static class DiscordExtensions
    {
        public static string ToHex(this uint color)
            => color == 0 ? "var(--light)" : "#" + color.ToString("X").PadLeft(6, '0');
    }
}
