﻿namespace ChipBot.Core.Web.Abstract.Interfaces
{
    /// <summary>
    /// Represents entry point for a plugin
    /// </summary>
    public interface IWebPlugin
    {
        /// <summary>
        /// Name of the plugin
        /// </summary>
        string Name { get; }
    }
}
