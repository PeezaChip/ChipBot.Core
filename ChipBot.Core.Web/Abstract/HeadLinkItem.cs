﻿namespace ChipBot.Core.Web.Abstract
{
    public class HeadLinkItem : HeadItem
    {
        public string Href { get; set; }
        public string Rel { get; set; }
    }
}
