﻿namespace ChipBot.Core.Web.Abstract
{
    public class PageCategory : NavItem
    {
        public List<PageInfo> Items { get; set; }
        public IOrderedEnumerable<PageInfo> Ordered => Items.OrderBy(p => p.Order);

        public bool Expanded = false;
    }
}
