﻿using Microsoft.AspNetCore.Components.Routing;

namespace ChipBot.Core.Web.Abstract
{
    public class PageInfo : NavItem
    {
        public PageCategory Category { get; set; }
        public string Link { get; set; }
        public NavLinkMatch NavLinkMatch { get; set; }
        public bool Hidden { get; set; }
        public bool OpenInNewTab { get; set; } = false;
    }
}
