﻿namespace ChipBot.Core.Web.Abstract
{
    public abstract class NavItem
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Roles { get; set; }
        public int Order { get; set; }
    }
}
