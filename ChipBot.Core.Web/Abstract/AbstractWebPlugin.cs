﻿using ChipBot.Core.Web.Abstract.Interfaces;

namespace ChipBot.Core.Web.Abstract
{
    /// <inheritdoc />
    public abstract class AbstractWebPlugin : IWebPlugin
    {
        /// <inheritdoc />
        public abstract string Name { get; }
    }
}
