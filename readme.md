# ChipBot.Core

It is a core for [ChipBot](https://gitlab.com/Eloncase/ChipBot) based on [Discord.Net](https://github.com/RogueException/Discord.Net) and offers slightly extended functionality. It can be used to create your own bot or a plugin for a ChipBot.

[Plugin Examples](https://gitlab.com/PeezaChip/ChipBot.Peeza.Plugins)