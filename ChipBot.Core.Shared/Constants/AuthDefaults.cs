﻿namespace ChipBot.Core.Shared.Constants
{
    public static class AuthDefaults
    {
        public const string Scheme = "ChipBot";
        public const string BearerScheme = "ChipBot.Token";
    }
}
