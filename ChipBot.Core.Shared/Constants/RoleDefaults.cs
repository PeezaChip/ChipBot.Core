﻿namespace ChipBot.Core.Shared.Constants
{
    public static class RoleDefaults
    {
        public const string Owner = "Owner";
        public const string Elite = "Elite";
    }
}
