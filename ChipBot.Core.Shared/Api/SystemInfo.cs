﻿namespace ChipBot.Core.Shared.Api
{
    public class SystemInfo
    {
        public string MachineName { get; set; }
        public string OS { get; set; }
        public string CPU { get; set; }
        public string Memory { get; set; }
        public DateTime ServerTime { get; set; }
        public DateTime ProcessStartTime { get; set; }

        public string Runtime { get; set; }
        public string DiscordNetVersion { get; set; }
        public string CoreVersion { get; set; }
        public string BotVersion { get; set; }

        public int GuildCount { get; set; }
        public int Channels { get; set; }
        public int Users { get; set; }
        public int Commands { get; set; }
    }
}
