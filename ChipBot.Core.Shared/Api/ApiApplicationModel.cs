﻿namespace ChipBot.Core.Shared.Api
{
    public class ApiApplicationModel
    {
        public bool IsEnabled { get; set; }

        public string Name { get; set; }

        public List<string> Scopes { get; set; } = new();
    }
}
