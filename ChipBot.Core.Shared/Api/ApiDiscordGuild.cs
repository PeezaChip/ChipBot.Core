﻿namespace ChipBot.Core.Shared.Api
{
    public class ApiDiscordGuild
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string IconUrl { get; set; }
    }
}
