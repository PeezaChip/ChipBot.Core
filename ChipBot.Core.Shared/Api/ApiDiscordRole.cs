﻿namespace ChipBot.Core.Shared.Api
{
    public class ApiDiscordRole
    {
        public uint Color { get; set; }

        public bool IsHoisted { get; set; }

        public bool IsManaged { get; set; }

        public bool IsMentionable { get; set; }

        public string Name { get; set; }

        public int Position { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public ulong Id { get; set; }

        public string Mention { get; set; }

        public ApiDiscordRole() { }

        public static ApiDiscordRole UnknownRole => new() { Id = 0, Name = "Unknown", Color = 5533306u };
    }
}
