﻿namespace ChipBot.Core.Shared.Api
{
    public class ApiApplicationExpandedModel : ApiApplicationModel
    {
        public string Key { get; set; }
    }
}
