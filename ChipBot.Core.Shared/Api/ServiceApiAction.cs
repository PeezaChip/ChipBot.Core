﻿namespace ChipBot.Core.Shared.Api
{
    public enum ServiceApiAction
    {
        restart,
        start,
        stop,
    }
}
