﻿namespace ChipBot.Core.Shared.Api
{
    public class UserInfo
    {
        public string Username { get; set; }
        public ulong UserId { get; set; }
        public string Avatar { get; set; }
        public string[] Roles { get; set; }
    }
}
