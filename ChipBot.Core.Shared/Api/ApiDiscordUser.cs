﻿namespace ChipBot.Core.Shared.Api
{
    public class ApiDiscordUser
    {
        public string AvatarId { get; set; }

        public string Discriminator { get; set; }

        public ushort DiscriminatorValue { get; set; }

        public bool IsBot { get; set; }

        public bool IsWebhook { get; set; }
        public bool IsStreaming { get; set; }

        public string Username { get; set; }
        public string Nickname { get; set; }
        public string DisplayName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? JoinedAt { get; set; }

        public ulong Id { get; set; }

        public string Mention { get; set; }

        public string AvatarUrl { get; set; }
        public string DefaultAvatarUrl { get; set; }

        public ulong[] RoleIds { get; set; }

        public ApiDiscordUser() { }

        public static ApiDiscordUser UnknownUser => new() { Id = 0, DiscriminatorValue = 0000, Discriminator = "0000", Nickname = "Unknown", Username = "Unknown", DisplayName = "Unknown", DefaultAvatarUrl = "https://cdn.discordapp.com/embed/avatars/0.png" };
    }
}
