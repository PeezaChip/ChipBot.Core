## v3.0.1.0 - 18.03.2023
### Added
- Auto migrate

## v3.0.0.2 - 15.03.2023
### Added
- Different abstract features for web
- Dry run mode

### Fixed
- Bug when service wasn't stopping

## v3.0.0.1 - 01.11.2022
### Added
- InteractionBase QoL changes

### Fixed
- An issue with service module

## v3.0.0.0 - 21.10.2022
### Added
- Interactions support

### Fixed
- Minor bugs

## v2.1.0.0 - 18.10.2022
### Changed
- Updated core dependencies
- NotifierService refactoring

## v2.0.1.1 - 29.08.2022
### Fixed
- Web service crash

## v2.0.1.0 - 10.11.2021
### Fixed
- Wrong version was displayed on a status message
- Fixed db concurrency exception in notifier service

## v2.0.0.0 - 03.11.2021
### Added
- `!feature` `!bug` commands
- Plugins can now modify serviceCollection
- Command interceptors
### Fixed
- Refactoring
- Commands should work in DMs again
### Changed
- The way logging works
- Command handling service is optimized
- Changed InteractiveBase
- Proper database
- Many minor changes
### Removed
- Roslyn service
- UTS

## v1.2.0.0 - 21.10.2020
### Fixed
- Minor refactoring
### Changed
- New versioning

## v1.2.0.0 - 21.10.2020
### Changed
- Changed target framework to netstandard
- New logo
### Added
- Timeout on service start

## v1.1.3.3 - 24.08.2020
### Fixed
- Timeout if service is taking a long time to start

## v1.1.3.2 - 05.08.2020
### Changed
- DependencyInjection dependency version bump
### Fixed
- Fix for nested modules

## v1.1.3.1 - 17.07.2020
### Added
- Reply error method

## v1.1.3.0 - 12.07.2020
### Added
- Interactive update
### Changed
- Updated scripting dependency

## v1.1.2.0 - 23.05.2020
### Changed
- `NotifierService` will now fire services in parallel

## v1.1.1.0 - 13.05.2020
### Added
- `INotifyService` interface
- `NotifierService` which will fire all implementation of `INotifyService` once a day
- `ChangelogService` which will post changelogs on start