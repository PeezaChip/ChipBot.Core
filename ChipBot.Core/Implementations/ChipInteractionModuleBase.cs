﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Core.Preconditions;
using Discord;
using Discord.Interactions;

namespace ChipBot.Core.Implementations
{
    [ChipInteractionPrecondition(typeof(InterceptorPrecondition))]
    public abstract class ChipInteractionModuleBase : InteractionModuleBase<ChipInteractionContext>, IChipModule
    {
        /// <inheritdoc/>
        public Task ReplyError(string message)
            => RespondAsync($":warning: {message}", ephemeral: true);

        /// <inheritdoc/>
        public Task ReplyEmbedAsync(Embed embed)
            => RespondAsync(embed: embed);

        /// <inheritdoc/>
        public Task ReplyEmbedAsync(EmbedBuilder builder)
            => ReplyEmbedAsync(builder.Build());

        /// <inheritdoc/>
        public Task ReplyReactionAsync(IEmote emote) => RespondAsync(emote.ToString());

        public Task ReplyReactionAsync(IEmote emote, bool ephemeral) => RespondAsync(emote.ToString(), ephemeral: ephemeral);

        /// <inheritdoc/>
        public Task ReplySuccessAsync() => ReplyReactionAsync(SuccessEmoji);

        public Task ReplySuccessAsync(bool ephemeral) => ReplyReactionAsync(SuccessEmoji, ephemeral);

        /// <inheritdoc/>
        public Task ReplyFailAsync() => ReplyReactionAsync(FailEmoji);

        public Task ReplyFailAsync(bool ephemeral) => ReplyReactionAsync(FailEmoji, ephemeral);

        public void RemoveMessageAndComponents(MessageProperties props)
        {
            props.Content = "No longer available.";
            props.Components = new ComponentBuilder().Build();
        }

        public void ChangeMessageAndRemoveComponents(MessageProperties props, string message)
        {
            props.Content = message;
            props.Components = new ComponentBuilder().Build();
        }

        private static readonly IEmote SuccessEmoji = new Emoji("👍");
        private static readonly IEmote FailEmoji = new Emoji("❌");
    }

}
