﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Interceptors;
using Discord.WebSocket;

namespace ChipBot.Core.Implementations
{
    public class ChipContext : IChipContext
    {
        public Dictionary<IInterceptor, object> InterceptorContext { get; private set; } = new();

        public SocketGuild Guild => context.Guild;

        public ISocketMessageChannel Channel => context.Channel;

        public SocketUser User => context.User;

        private readonly IDiscordContext context;

        public ChipContext(IDiscordContext context) => this.context = context;
    }
}
