﻿using IResult = Discord.Interactions.IResult;

namespace ChipBot.Core.Implementations
{
    public class ChipExecutionResult
    {
        public string ErrorReason { get; }
        public bool IsSuccess { get; }

        private ChipExecutionResult(string error, bool success)
        {
            ErrorReason = error;
            IsSuccess = success;
        }

        public static ChipExecutionResult FromResult(IResult result)
            => new(result.ErrorReason, result.IsSuccess);
    }
}
