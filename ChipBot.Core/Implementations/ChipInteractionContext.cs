﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Interceptors;
using Discord.Interactions;
using Discord.WebSocket;

namespace ChipBot.Core.Implementations
{
    public class ChipInteractionContext : SocketInteractionContext, IChipGenericContext<SocketInteraction>
    {
        public Dictionary<IInterceptor, object> InterceptorContext => chipContext.InterceptorContext;

        public SocketInteraction Data => Interaction;

        private readonly ChipContext chipContext;

        public ChipInteractionContext(DiscordSocketClient client, SocketInteraction interaction)
            : base(client, interaction)
            => chipContext = new(this);
    }
}
