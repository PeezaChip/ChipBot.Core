﻿using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;

namespace ChipBot.Core.Extensions
{
    public static class HttpExtensions
    {
        public static ulong GetDiscordUserId(this ClaimsPrincipal principal)
            => ulong.Parse(principal.FindFirstValue(ClaimTypes.NameIdentifier));

        public static ulong GetCurrentDiscordUserId(this HttpContext context)
            => context.User.GetDiscordUserId();

        public static ulong GetCurrentDiscordUserId(this HubCallerContext context)
            => context.User.GetDiscordUserId();

        public static IUser GetCurrentDiscordUser(this HttpContext context)
        {
            var discord = context.RequestServices.GetRequiredService<DiscordSocketClient>();
            return discord.GetUser(context.GetCurrentDiscordUserId());
        }

        public static IUser GetCurrentDiscordUser(this HubCallerContext context)
            => context.GetHttpContext().GetCurrentDiscordUser();
    }
}
