﻿using Microsoft.EntityFrameworkCore;

namespace ChipBot.Core.Extensions
{
    public static class DbContextExtensions
    {
        public static void AddOrUpdate<T>(this DbSet<T> dbSet, T entity, params object[] key) where T : class
        {
            var current = dbSet.Find(key);
            if (current != null) dbSet.Remove(current);
            dbSet.Add(entity);
        }
    }
}
