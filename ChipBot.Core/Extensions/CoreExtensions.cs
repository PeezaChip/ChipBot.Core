﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration;
using ChipBot.Core.Constants;
using ChipBot.Core.Data;
using ChipBot.Core.DependencyInjection;
using ChipBot.Core.Middleware;
using ChipBot.Core.Policies.Requirements;
using ChipBot.Core.Services;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Shared.Constants;
using Discord;
using Discord.Commands;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using static ChipBot.Core.DependencyInjection.DiscordInjections;

namespace ChipBot.Core.Extensions
{
    public static class CoreExtensions
    {
        public static void AddChipCore(this IHostApplicationBuilder builder, List<Assembly> assemblies)
        {
            assemblies.Add(Assembly.GetExecutingAssembly());

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var types = assemblies
                .SelectMany(a => a.ExportedTypes);

            builder.Configuration.Add(new DatabaseConfigurationSource());

            builder.Services
                .AddAuthentication(AuthDefaults.Scheme)
                .AddCookie(AuthDefaults.Scheme, options =>
                {
                    options.Cookie.Name = "ChipWebCookie";
                })
                .AddJwtBearer(AuthDefaults.BearerScheme);

            builder.Services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(AuthDefaults.Scheme, AuthDefaults.BearerScheme)
                    .RequireAuthenticatedUser()
                    .RequireValidScopes()
                    .Build();
            });

            builder.Services.ConfigureOptions<ConfigureJwtBearerOptions>();

            GetAllHubTypes(types);
            if (chipHubs.Any())
            {
                builder.Services.AddSignalR();
            }

            builder.Services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                   new[] { "application/octet-stream" });
            });

            GetAllApiDescriptions(types);
            builder.Services
                .AddControllers(options =>
                {
                    options.CacheProfiles.Add(CachingConstants.NoCache, new() { NoStore = true, Duration = 0, Location = ResponseCacheLocation.None });

                    options.CacheProfiles.Add(CachingConstants.MinuteUserCache, new() { NoStore = false, Duration = 60, Location = ResponseCacheLocation.Client });
                    options.CacheProfiles.Add(CachingConstants.FiveMinutesUserCache, new() { NoStore = false, Duration = 60 * 5, Location = ResponseCacheLocation.Client });
                    options.CacheProfiles.Add(CachingConstants.HourUserCache, new() { NoStore = false, Duration = 60 * 60, Location = ResponseCacheLocation.Client });
                    options.CacheProfiles.Add(CachingConstants.DayUserCache, new() { NoStore = false, Duration = 60 * 60 * 24, Location = ResponseCacheLocation.Client });

                    options.CacheProfiles.Add(CachingConstants.MinutePublicCache, new() { NoStore = false, Duration = 60, Location = ResponseCacheLocation.Any });
                    options.CacheProfiles.Add(CachingConstants.FiveMinutesPublicCache, new() { NoStore = false, Duration = 60 * 5, Location = ResponseCacheLocation.Any });
                    options.CacheProfiles.Add(CachingConstants.HourPublicCache, new() { NoStore = false, Duration = 60 * 60, Location = ResponseCacheLocation.Any });
                    options.CacheProfiles.Add(CachingConstants.DayPublicCache, new() { NoStore = false, Duration = 60 * 60 * 24, Location = ResponseCacheLocation.Any });
                })
                .AddApplicationPart(Assembly.GetExecutingAssembly())
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                foreach (var chipApi in chipApis)
                {
                    options.SwaggerDoc(chipApi.GroupName, new OpenApiInfo { Title = chipApi.Title, Version = chipApi.Version });
                }
            });
            builder.Services.AddSingleton(chipApis);
            builder.Services.AddProblemDetails();

            builder.Services
                .AddLogging(o => o.AddConsole());

            builder.Services
                .AddSingleton(typeof(SettingsService<>))
                .AddSingleton(typeof(UserSettingsService<>));

            builder.Services
                .AddSingleton(typeof(ChipBotDbContextFactory))
                .AddSingleton(typeof(ChipBotDbContextFactory<>));

            builder.Services
                .AddSingleton(new DiscordSocketClient(new DiscordSocketConfig()
                {
                    LogLevel = LogSeverity.Error,
                    AlwaysDownloadUsers = true,
                    GatewayIntents = GatewayIntents.All
                }))
                .AddSingleton(new CommandService(new CommandServiceConfig
                {
                    LogLevel = LogSeverity.Debug,
                    DefaultRunMode = Discord.Commands.RunMode.Async,
                    CaseSensitiveCommands = false
                }))
                .AddSingleton<InteractionService>()

                .AddSingleton<Random>()

                .AddMemoryCache();

            builder.Services
                .AddTypeReaders(types)
                .AddInteractionModules(types)
                .AddDbContexts(types)
                .AddServices(types)
                .AddPlugins(builder.Configuration, types);
        }

        public static void UseChipCore(this WebApplication app)
        {
            ApplyMigrations(app.Services);

            app.UseResponseCompression();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                foreach (var chipApi in chipApis)
                {
                    options.SwaggerEndpoint(chipApi.SwaggerLink, chipApi.Title);
                }
            });

            if (app.Environment.IsDevelopment())
            {
                app.UseWebAssemblyDebugging();
            }

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ClaimsRefreshMiddleware>();

            app.MapControllers();
            app.MapHubs();
            app.MapFallbackToFile("index.html");

            var startup = app.Services.GetRequiredService<StartupService>();
            _ = startup.StartAsync();
        }

        private static void MapHubs(this WebApplication app)
        {
            var mapMethod = typeof(HubEndpointRouteBuilderExtensions).GetMethod(
                nameof(HubEndpointRouteBuilderExtensions.MapHub),
                new Type[] { typeof(IEndpointRouteBuilder), typeof(string) });

            foreach (var hub in chipHubs)
            {
                var attr = hub.GetCustomAttribute<ChipHubAttribute>();
                mapMethod.MakeGenericMethod(hub).Invoke(null, new object[] { app, attr.Path });
            }
        }

        private static List<Type> chipHubs;
        private static void GetAllHubTypes(IEnumerable<Type> types)
        {
            chipHubs = types
                .Where(t => t.GetCustomAttribute<ChipHubAttribute>() != null)
                .ToList();
        }

        private static List<ChipApiAttribute> chipApis;
        private static void GetAllApiDescriptions(IEnumerable<Type> types)
        {
            var chipApisAll = types
                .Where(t => t.GetCustomAttribute<ChipApiAttribute>() != null)
                .Select(t => t.GetCustomAttribute<ChipApiAttribute>());

            chipApis = chipApisAll
                .GroupBy(c => c.GroupKey)
                .Select(g => g.FirstOrDefault())
                .ToList();
        }

        private static void ApplyMigrations(IServiceProvider provider)
        {
            var contexts = provider.GetService<ContextPackage>();
            var genericFactory = typeof(ChipBotDbContextFactory<>);
            foreach (var context in contexts.Types)
            {
                var typedFactory = genericFactory.MakeGenericType(context);
                var chipDbFactory = Activator.CreateInstance(typedFactory) as IChipDbFactory;
                chipDbFactory.ApplyMigrationIfNeeded();
            }
        }

        private static AuthorizationPolicyBuilder RequireValidScopes(this AuthorizationPolicyBuilder authorizationPolicyBuilder)
        {
            authorizationPolicyBuilder.Requirements.Add(new ValidScopesRequirement());
            return authorizationPolicyBuilder;
        }
    }
}
