﻿using ChipBot.Core.Abstract.Interfaces;
using Discord;
using Discord.Interactions;
using System.Reflection;
using System.Text;

namespace ChipBot.Core.Extensions
{
    public static class CommonExtensions
    {
        /// <summary>
        /// Shuffles a <paramref name="sequence"/> of <typeparamref name="T"/> elements
        /// </summary>
        /// <typeparam name="T">The type of objects in a sequence</typeparam>
        /// <param name="sequence">sequence of elements</param>
        /// <param name="random">instance of <see cref="Random"/> class</param>
        /// <returns>Sequence of elements ordered in a random way</returns>
        public static IEnumerable<T> OrderRandomly<T>(this IEnumerable<T> sequence, Random random)
        {
            var copy = sequence.ToList();

            while (copy.Count > 0)
            {
                var index = random.Next(copy.Count);
                yield return copy[index];
                copy.RemoveAt(index);
            }
        }

        public static IEnumerable<IEnumerable<T>> MakeGroupsOf<T>(this IEnumerable<T> source, int count)
        {
            var grouping = new List<T>();
            foreach (var item in source)
            {
                grouping.Add(item);
                if (grouping.Count == count)
                {
                    yield return grouping;
                    grouping = new List<T>();
                }
            }

            if (grouping.Count != 0)
            {
                yield return grouping;
            }
        }

        public static string GetRealTypeName(this Type t)
        {
            if (!t.IsGenericType)
                return t.Name;

            StringBuilder sb = new();
            sb.Append(t.Name[..t.Name.IndexOf('`')]);
            sb.Append('<');
            var appendComma = false;
            foreach (var arg in t.GetGenericArguments())
            {
                if (appendComma) sb.Append(',');
                sb.Append(GetRealTypeName(arg));
                appendComma = true;
            }
            sb.Append('>');
            return sb.ToString();
        }

        public static string GetVersion(this IVersionable versionable)
            => versionable.GetType().Assembly.GetVersion();

        public static string GetVersion(this Assembly assembly)
            => assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

        public static string ToDBKey(this ICommandInfo commandInfo)
            => $"{(string.IsNullOrWhiteSpace(commandInfo.Module.SlashGroupName) ? "" : $"{commandInfo.Module.SlashGroupName}.")}{commandInfo.Name}({string.Join(",", commandInfo.Parameters.Select(p => p.Name))})".ToLower();

        public static EmbedBuilder GetDefaultCancelledEmbed(this EmbedBuilder embedBuilder)
            => embedBuilder.WithDescription("No longer available :c");
    }
}
