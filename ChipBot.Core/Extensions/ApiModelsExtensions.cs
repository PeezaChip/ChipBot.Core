﻿using ChipBot.Core.Data.Models;
using ChipBot.Core.Shared.Api;
using Discord;
using Discord.WebSocket;
using System.Data;

namespace ChipBot.Core.Extensions
{
    public static class ApiModelsExtensions
    {
        public static ApiDiscordUser ToApiDiscordUser(this SocketGuildUser user)
        {
            return new ApiDiscordUser()
            {
                AvatarId = user.AvatarId,
                Discriminator = user.Discriminator,
                DiscriminatorValue = user.DiscriminatorValue,
                IsBot = user.IsBot,
                IsWebhook = user.IsWebhook,
                IsStreaming = user.VoiceState?.IsStreaming ?? false,
                Username = user.Username,
                Nickname = user.Username,
                DisplayName = user.DisplayName,
                CreatedAt = user.CreatedAt,
                JoinedAt = user.JoinedAt,
                Id = user.Id,
                Mention = user.Mention,
                AvatarUrl = user.GetAvatarUrl(ImageFormat.WebP, 512),
                DefaultAvatarUrl = user.GetDefaultAvatarUrl(),
                RoleIds = user.Roles.Select(r => r.Id).ToArray()
            };
        }

        public static ApiDiscordRole ToApiDiscordRole(this SocketRole role)
        {
            return new ApiDiscordRole()
            {
                Color = role.Color.RawValue,
                IsHoisted = role.IsHoisted,
                IsManaged = role.IsManaged,
                IsMentionable = role.IsMentionable,
                Name = role.Name,
                Position = role.Position,
                CreatedAt = role.CreatedAt,
                Id = role.Id,
                Mention = role.Mention
            };
        }

        public static ApiDiscordGuild ToApiDiscordGuild(this SocketGuild guild)
        {
            return new ApiDiscordGuild()
            {
                Id = guild.Id,
                IconUrl = guild.IconUrl,
                Name = guild.Name
            };
        }

        public static ApiApplicationModel ToApiApplication(this ApiApplication apiApp)
        {
            return new ApiApplicationModel()
            {
                IsEnabled = apiApp.IsEnabled,
                Name = apiApp.Name,
                Scopes = apiApp.Scopes
            };
        }

        public static ApiApplicationExpandedModel ToApiExpandedApplication(this ApiApplication apiApp)
        {
            return new ApiApplicationExpandedModel()
            {
                IsEnabled = apiApp.IsEnabled,
                Name = apiApp.Name,
                Scopes = apiApp.Scopes,
            };
        }
    }
}
