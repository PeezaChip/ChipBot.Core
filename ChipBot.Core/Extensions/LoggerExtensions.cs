﻿using Discord;
using Microsoft.Extensions.Logging;

namespace ChipBot.Core.Extensions
{
    public static class LoggerExtensions
    {
        public static void LogDiscordMessage(this ILogger logger, LogMessage message)
        {
            using var scope = logger.BeginScope(message.Source);
            var args = message.Exception != null ? new object[] { message.Exception, message.Message } : new object[] { message.Message };
            switch (message.Severity)
            {
                case LogSeverity.Critical:
                    logger.LogCritical(message.Exception, message.Message);
                    break;

                case LogSeverity.Error:
                    logger.LogError(message.Exception, message.Message);
                    break;

                case LogSeverity.Warning:
                    logger.LogWarning(message.Exception, message.Message);
                    break;

                case LogSeverity.Info:
                    logger.LogInformation(message.Exception, message.Message);
                    break;

                case LogSeverity.Verbose:
                    logger.LogTrace(message.Exception, message.Message);
                    break;

                case LogSeverity.Debug:
                    logger.LogDebug(message.Exception, message.Message);
                    break;
            }
        }
    }
}
