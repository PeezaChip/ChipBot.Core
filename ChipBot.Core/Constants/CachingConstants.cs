﻿namespace ChipBot.Core.Constants
{
    public static class CachingConstants
    {
        public const string NoCache = "NoCache";

        public const string MinuteUserCache = "minUserCache";
        public const string FiveMinutesUserCache = "5minUserCache";
        public const string HourUserCache = "HourUserCache";
        public const string DayUserCache = "DayUserCache";

        public const string MinutePublicCache = "minPublicCache";
        public const string FiveMinutesPublicCache = "5minPublicCache";
        public const string HourPublicCache = "HourPublicCache";
        public const string DayPublicCache = "DayPublicCache";
    }
}
