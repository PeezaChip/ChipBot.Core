﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Implementations;

namespace ChipBot.Core.Interceptors
{
    public abstract class AbstractInterceptor : IInterceptor
    {
        public abstract bool ShouldIntercept(IChipContext context, object command);

        public abstract Task<InterceptResult> Intercept(IChipContext context, string commandKey);

        public virtual Task OnCommandError(IChipContext context, string commandKey, ChipExecutionResult result)
        {
            return Task.CompletedTask;
        }
    }
}
