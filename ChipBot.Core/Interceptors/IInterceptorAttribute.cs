﻿namespace ChipBot.Core.Interceptors
{
    public interface IInterceptorAttribute<T> where T : class
    {
        public abstract T ToMeta();
    }
}
