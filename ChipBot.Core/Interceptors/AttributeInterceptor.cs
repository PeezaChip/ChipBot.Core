﻿using ChipBot.Core.Abstract.Interfaces;
using Discord.Commands;
using Discord.Interactions;

namespace ChipBot.Core.Interceptors
{
    public abstract class AttributeInterceptor<TAttr, TMeta> : AbstractInterceptor where TAttr : Attribute, IInterceptorAttribute<TMeta> where TMeta : class
    {
        public override bool ShouldIntercept(IChipContext context, object command)
        {
            var attributes = command is CommandInfo cmdInfo ? cmdInfo.Attributes : command is ICommandInfo interInfo ? interInfo.Attributes : null;
            if (attributes == null) return false;

            var attr = attributes.FirstOrDefault(a => a is TAttr);

            SetContext(context, attr);

            return attr != null;
        }

        protected void SetContext(IChipContext context, Attribute attr)
        {
            if (attr == null) return;
            context.InterceptorContext.Add(this, ((IInterceptorAttribute<TMeta>)attr).ToMeta());
        }

        protected TMeta GetMeta(IChipContext context)
            => (TMeta)context.InterceptorContext[this];

        protected void RemoveMeta(IChipContext context)
            => context.InterceptorContext.Remove(this);
    }
}
