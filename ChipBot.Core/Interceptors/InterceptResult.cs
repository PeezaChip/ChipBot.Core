﻿namespace ChipBot.Core.Interceptors
{
    public struct InterceptResult
    {
        public string Reason { get; private set; }
        public bool IsIntercepted { get; private set; }

        public InterceptResult(string reason = null)
        {
            if (string.IsNullOrWhiteSpace(reason))
            {
                Reason = null;
                IsIntercepted = false;
            }
            else
            {
                Reason = reason;
                IsIntercepted = true;
            }
        }
    }
}
