﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Implementations;

namespace ChipBot.Core.Interceptors
{
    public interface IInterceptor
    {
        public bool ShouldIntercept(IChipContext context, object command);
        public Task<InterceptResult> Intercept(IChipContext context, string commandKey);
        public Task OnCommandError(IChipContext context, string commandKey, ChipExecutionResult result);
    }
}
