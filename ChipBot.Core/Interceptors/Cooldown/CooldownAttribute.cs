﻿namespace ChipBot.Core.Interceptors.Cooldown
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CooldownAttribute : Attribute, IInterceptorAttribute<CooldownMeta>
    {
        private CooldownMeta Meta { get; set; } = new CooldownMeta();

        public int CooldownMs { get => Meta.CooldownMs; private set => Meta.CooldownMs = value; }
        public bool Global { get => Meta.Global; set => Meta.Global = value; }

        public CooldownAttribute(int ms) => CooldownMs = ms;

        public CooldownMeta ToMeta() => Meta;
    }
}
