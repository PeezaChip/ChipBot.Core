﻿namespace ChipBot.Core.Interceptors.Cooldown
{
    public class CooldownMeta
    {
        public int CooldownMs { get; set; } = 0;
        public bool Global { get; set; } = false;
    }
}
