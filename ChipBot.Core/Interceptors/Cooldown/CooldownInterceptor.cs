﻿using ChipBot.Core.Abstract.Interfaces;
using Discord;
using Microsoft.Extensions.Caching.Memory;

namespace ChipBot.Core.Interceptors.Cooldown
{
    public class CooldownInterceptor : AttributeInterceptor<CooldownAttribute, CooldownMeta>
    {
        private readonly MemoryCache cache;

        public CooldownInterceptor(MemoryCache cache)
        {
            this.cache = cache;
        }
        public override Task<InterceptResult> Intercept(IChipContext context, string commandKey)
        {
            var meta = GetMeta(context);
            if (meta == null) return Task.FromResult(new InterceptResult());
            RemoveMeta(context);

            if (IsExecutionAllowed(meta, context, commandKey))
            {
                StoreToCache(meta, context, commandKey);
                return Task.FromResult(new InterceptResult());
            }
            else
            {
                return Task.FromResult(new InterceptResult("Command is on cooldown"));
            }
        }

        private void StoreToCache(CooldownMeta meta, IChipContext context, string commandKey)
        {
            if (meta.CooldownMs <= 0) return;
            var cacheKey = meta.Global ? GetGlobalCacheKey(commandKey) : GetUserCacheKey(commandKey, context.User);
            cache.Set(cacheKey, 0, TimeSpan.FromMilliseconds(meta.CooldownMs));
        }

        private bool IsExecutionAllowed(CooldownMeta meta, IChipContext context, string commandKey)
        {
            var cacheValue = meta.Global ? (int?)cache.Get(GetGlobalCacheKey(commandKey)) : (int?)cache.Get(GetUserCacheKey(commandKey, context.User));
            return !cacheValue.HasValue;
        }

        private string GetUserCacheKey(string commandKey, IUser user) => $"cooldown_u{user.Id}_{commandKey}";
        private string GetGlobalCacheKey(string commandKey) => $"cooldown_global_{commandKey}";

    }
}
