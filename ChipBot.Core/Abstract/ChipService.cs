﻿using ChipBot.Core.Extensions;
using Discord;
using Microsoft.Extensions.Logging;

namespace ChipBot.Core.Abstract
{
    /// <summary>
    /// Represents base class for all services
    /// </summary>
    public abstract class ChipService<T>
    {
        /// <summary>
        /// Logging service
        /// </summary>
        protected ILogger<T> logger;

        /// <summary>
        /// Logs the message to console
        /// </summary>
        /// <param name="severity">severity of the log message</param>
        /// <param name="msg">msg to be logged</param>
        /// <param name="ex">exception if any</param>
        [Obsolete("Use logger directly")]
        protected void Log(LogSeverity severity, string msg, Exception ex = null)
            => logger.LogDiscordMessage(new LogMessage(severity, GetType().Name, msg, ex));

        /// <summary>
        /// Initializes a new <see cref="ChipService{T}"/> instance
        /// </summary>
        /// <param name="log">Logging service</param>
        protected ChipService(ILogger<T> log)
            => logger = log;
    }
}
