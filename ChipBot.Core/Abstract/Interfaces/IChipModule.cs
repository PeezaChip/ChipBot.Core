﻿using Discord;

namespace ChipBot.Core.Abstract.Interfaces
{
    public interface IChipModule
    {
        /// <summary>
        /// Replies with error message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task ReplyError(string message);

        /// <summary>
        /// Sends embed
        /// </summary>
        /// <param name="embed">Embed that will be sent</param>
        /// <returns>Task</returns>
        Task ReplyEmbedAsync(Embed embed);

        /// <summary>
        /// Builds an embed and sends it
        /// </summary>
        /// <param name="builder">Embed builder that will be used to build embed</param>
        /// <returns>Task</returns>
        Task ReplyEmbedAsync(EmbedBuilder builder);

        /// <summary>
        /// Replies with custom emote
        /// </summary>
        /// <param name="emote">Emote that will be attached to a message</param>
        /// <returns>Task</returns>
        public Task ReplyReactionAsync(IEmote emote);

        /// <summary>
        /// Replies with success reaction
        /// </summary>
        /// <returns>Task</returns>
        public Task ReplySuccessAsync();

        /// <summary>
        /// Replies with fail reaction
        /// </summary>
        /// <returns>Task</returns>
        public Task ReplyFailAsync();
    }
}
