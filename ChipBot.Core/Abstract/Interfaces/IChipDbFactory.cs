﻿namespace ChipBot.Core.Abstract.Interfaces
{
    public interface IChipDbFactory
    {
        void ApplyMigrationIfNeeded();
    }
}
