﻿using Discord.WebSocket;

namespace ChipBot.Core.Abstract.Interfaces
{
    public interface IDiscordContext
    {
        public SocketGuild Guild { get; }
        public SocketUser User { get; }
        public ISocketMessageChannel Channel { get; }
    }
}
