﻿namespace ChipBot.Core.Abstract.Interfaces
{
    /// <summary>
    /// Bot interface <see cref="Modules.HelpModule.InfoAsync"/>
    /// </summary>
    public interface IBot : IVersionable
    {
        /// <summary>
        /// Bot created time in Unix Time Milliseconds
        /// </summary>
        long CreatedTS { get; }
        /// <summary>
        /// Bot description
        /// </summary>
        string Description { get; }
        /// <summary>
        /// Bot thumbnail
        /// </summary>
        string Thumbnail { get; }
    }
}
