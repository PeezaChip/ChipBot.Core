﻿namespace ChipBot.Core.Abstract.Interfaces
{
    /// <summary>
    /// Interface showing that service can be turned off/on
    /// </summary>
    public interface IBackgroundService
    {
        /// <summary>
        /// Starts a service
        /// </summary>
        /// <returns>Task</returns>
        Task StartAsync();
        /// <summary>
        /// Stops a service
        /// </summary>
        /// <returns>Task</returns>
        Task StopAsync();
        /// <summary>
        /// Returns a value indicating service status
        /// </summary>
        bool IsWorking { get; }
    }
}
