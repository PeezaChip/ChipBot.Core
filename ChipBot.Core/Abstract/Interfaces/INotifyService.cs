﻿namespace ChipBot.Core.Abstract.Interfaces
{
    /// <summary>
    /// Interface that shows that service needs to be invoked once a day
    /// </summary>
    public interface INotifyService
    {
        /// <summary>
        /// Method that will be invoked once a day
        /// </summary>
        /// <returns>Task</returns>
        Task Notify();
    }
}
