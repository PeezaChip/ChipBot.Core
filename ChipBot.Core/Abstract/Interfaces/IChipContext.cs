﻿using ChipBot.Core.Interceptors;

namespace ChipBot.Core.Abstract.Interfaces
{
    public interface IChipContext : IDiscordContext
    {
        public Dictionary<IInterceptor, object> InterceptorContext { get; }
    }
}
