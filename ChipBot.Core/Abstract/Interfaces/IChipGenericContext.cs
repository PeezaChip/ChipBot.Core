﻿namespace ChipBot.Core.Abstract.Interfaces
{
    public interface IChipGenericContext<TData> : IChipContext
    {
        public TData Data { get; }
    }
}
