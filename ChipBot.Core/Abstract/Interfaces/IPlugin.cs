﻿namespace ChipBot.Core.Abstract.Interfaces
{
    /// <summary>
    /// Represents entry point for a plugin
    /// </summary>
    public interface IPlugin : IVersionable
    {
        /// <summary>
        /// Name of the plugin
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Method to modify bot service collection
        /// </summary>
        /// <param name="serviceCollection">ServiceCollection</param>
        void ModifyServiceCollection(IConfiguration configuration, IServiceCollection serviceCollection);
    }
}
