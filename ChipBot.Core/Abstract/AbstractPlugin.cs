﻿using ChipBot.Core.Abstract.Interfaces;

namespace ChipBot.Core.Abstract
{
    /// <inheritdoc />
    public abstract class AbstractPlugin : IPlugin
    {
        /// <inheritdoc />
        public abstract string Name { get; }

        /// <inheritdoc />
        public virtual void ModifyServiceCollection(IConfiguration configuration, IServiceCollection serviceCollection) { }
    }
}
