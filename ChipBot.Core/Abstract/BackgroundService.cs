﻿using ChipBot.Core.Abstract.Interfaces;
using Microsoft.Extensions.Logging;

namespace ChipBot.Core.Abstract
{
    public abstract class BackgroundService<T> : ChipService<T>, IBackgroundService
    {
        public abstract Task StartAsync();
        public abstract Task StopAsync();
        public abstract bool IsWorking { get; }

        protected BackgroundService(ILogger<T> logger) : base(logger) { }
    }
}
