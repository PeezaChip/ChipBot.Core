﻿using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using Microsoft.Extensions.Logging;

namespace ChipBot.Core.Abstract
{
    /// <summary>
    /// Abstract class for making an API services
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ApiService<T> : ChipService<ApiService<T>>
    {
        /// <summary>
        /// ApiClient that will be exposed
        /// </summary>
        public T ApiClient { get; protected set; }

        /// <summary>
        /// UserAgent that is used to pass along with API requests
        /// </summary>
        protected string UserAgent => NetworkConfig.Settings.UserAgent;

        protected SettingsService<NetworkConfig> NetworkConfig { get; private set; }

        /// <summary>
        /// Initializes a new <see cref="ApiService{T}"/> class
        /// </summary>
        /// <param name="logger">LoggingService</param>
        /// <param name="globalSettings">global settings provider</param>
        protected ApiService(ILogger<ApiService<T>> logger, SettingsService<NetworkConfig> networkConfig) : base(logger)
            => NetworkConfig = networkConfig;
    }
}
