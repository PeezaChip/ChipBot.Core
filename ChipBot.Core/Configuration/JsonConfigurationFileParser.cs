﻿using ChipBot.Core.Data.Models;
using System.Diagnostics;
using System.Text.Json;

namespace ChipBot.Core.Configuration
{
    // https://github.com/dotnet/runtime/blob/a3a0f5cf2223026e3518ab8f2028b4db6ed375cd/src/libraries/Microsoft.Extensions.Configuration.Json/src/JsonConfigurationFileParser.cs
    internal sealed class JsonConfigurationFileParser
    {
        private JsonConfigurationFileParser() { }

        private readonly Dictionary<string, string> _data = new(StringComparer.OrdinalIgnoreCase);
        private readonly Stack<string> _paths = new();

        public static IDictionary<string, string> Parse(IEnumerable<ChipSettings> settings, IEnumerable<ChipUserSettings> userSettings)
            => new JsonConfigurationFileParser().ParseStream(settings, userSettings);

        private Dictionary<string, string> ParseStream(IEnumerable<ChipSettings> settings, IEnumerable<ChipUserSettings> userSettings)
        {
            var jsonDocumentOptions = new JsonDocumentOptions
            {
                CommentHandling = JsonCommentHandling.Skip,
                AllowTrailingCommas = true,
            };

            foreach (var setting in settings)
            {
                using var doc = JsonDocument.Parse(setting.Json, jsonDocumentOptions);
                EnterContext(setting.Name);
                VisitObjectElement(doc.RootElement);
                ExitContext();
            }

            foreach (var userSetting in userSettings)
            {
                using var doc = JsonDocument.Parse(userSetting.Json, jsonDocumentOptions);
                EnterContext(userSetting.Name);
                EnterContext(userSetting.UserId.ToString());
                VisitObjectElement(doc.RootElement);
                ExitContext();
                ExitContext();
            }

            return _data;
        }

        private void VisitObjectElement(JsonElement element)
        {
            var isEmpty = true;

            foreach (var property in element.EnumerateObject())
            {
                isEmpty = false;
                EnterContext(property.Name);
                VisitValue(property.Value);
                ExitContext();
            }

            SetNullIfElementIsEmpty(isEmpty);
        }

        private void VisitArrayElement(JsonElement element)
        {
            var index = 0;

            foreach (var arrayElement in element.EnumerateArray())
            {
                EnterContext(index.ToString());
                VisitValue(arrayElement);
                ExitContext();
                index++;
            }

            SetNullIfElementIsEmpty(isEmpty: index == 0);
        }

        private void SetNullIfElementIsEmpty(bool isEmpty)
        {
            if (isEmpty && _paths.Count > 0)
            {
                _data[_paths.Peek()] = null;
            }
        }

        private void VisitValue(JsonElement value)
        {
            Debug.Assert(_paths.Count > 0);

            switch (value.ValueKind)
            {
                case JsonValueKind.Object:
                    VisitObjectElement(value);
                    break;

                case JsonValueKind.Array:
                    VisitArrayElement(value);
                    break;

                case JsonValueKind.Number:
                case JsonValueKind.String:
                case JsonValueKind.True:
                case JsonValueKind.False:
                case JsonValueKind.Null:
                    var key = _paths.Peek();
                    if (_data.ContainsKey(key))
                    {
                        throw new FormatException();
                    }
                    _data[key] = value.ToString();
                    break;

                default:
                    throw new FormatException();
            }
        }

        private void EnterContext(string context) =>
            _paths.Push(_paths.Count > 0 ?
                _paths.Peek() + ConfigurationPath.KeyDelimiter + context :
                context);

        private void ExitContext() => _paths.Pop();
    }
}
