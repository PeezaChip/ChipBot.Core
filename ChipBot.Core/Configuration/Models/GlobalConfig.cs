﻿namespace ChipBot.Core.Configuration.Models
{
    public class GlobalConfig
    {
        public ulong MainGuildId { get; set; }
        public ulong DevGuildId { get; set; }
        public List<ulong> SecondaryGuildIds { get; set; } = new();

        public bool IsDebug { get; set; }
        public bool RestrictUsers { get; set; }
        public bool RestrictGuilds { get; set; }

        public List<ulong> Owners { get; set; } = new();
        public List<ulong> EliteRoleIds { get; set; } = new();

        public List<ulong> AllowedChannelIds { get; set; } = new();
    }
}
