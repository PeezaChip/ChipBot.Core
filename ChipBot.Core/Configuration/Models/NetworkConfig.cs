﻿namespace ChipBot.Core.Configuration.Models
{
    public class NetworkConfig
    {
        public string UserAgent { get; set; }

        public string Proto { get; set; }

        public string Host { get; set; }

        public short Port { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
