﻿namespace ChipBot.Core.Configuration.Models
{
    internal class InteractionHandlingServiceConfig
    {
        public bool AutoRegisterCommands { get; set; }
    }
}
