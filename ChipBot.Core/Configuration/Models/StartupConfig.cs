﻿namespace ChipBot.Core.Configuration.Models
{
    internal class StartupConfig
    {
        public string DiscordToken { get; set; }

        public ulong StatusChannelId { get; set; }

        public int SupressDowntimeLessThanMinutes { get; set; }

        public List<string> StartOnReady { get; set; } = new();
        public List<string> RequireServices { get; set; } = new();
    }
}
