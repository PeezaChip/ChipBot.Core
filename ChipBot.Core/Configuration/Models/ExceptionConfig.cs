﻿namespace ChipBot.Core.Configuration.Models
{
    internal class ExceptionConfig
    {
        public ulong ExceptionChannelId { get; set; }
        public List<string> IgnoreExceptionsFrom { get; set; } = new();
    }
}
