﻿namespace ChipBot.Core.Configuration.Models
{
    internal class NotifyConfig
    {
        public List<string> NotifyServices { get; set; }
    }
}
