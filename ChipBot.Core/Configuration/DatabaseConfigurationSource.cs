﻿using ChipBot.Core.Data;

namespace ChipBot.Core.Configuration
{
    internal class DatabaseConfigurationSource : IConfigurationSource
    {
        public DatabaseConfigurationSource() { }

        public IConfigurationProvider Build(IConfigurationBuilder builder)
            => new DatabaseConfigurationProvider(new ChipBotDbContextFactory());
    }
}
