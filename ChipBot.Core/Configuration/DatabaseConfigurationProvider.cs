﻿using ChipBot.Core.Data;

namespace ChipBot.Core.Configuration
{
    internal class DatabaseConfigurationProvider : ConfigurationProvider
    {
        private readonly ChipBotDbContextFactory dbFactory;
        private bool first = true;

        public DatabaseConfigurationProvider(ChipBotDbContextFactory dbFactory) => this.dbFactory = dbFactory;

        public override void Load()
        {
            using var dbContext = dbFactory.CreateDbContext();
            try
            {
                Data = JsonConfigurationFileParser.Parse(dbContext.Settings, dbContext.UserSettings);
            }
            catch { }

            if (first)
            {
                first = false;
                _ = InitReload();
            }
        }

        private async Task InitReload()
        {
            while (true)
            {
                await Task.Delay(TimeSpan.FromMinutes(5));
                Load();
                OnReload();
            }
        }
    }
}
