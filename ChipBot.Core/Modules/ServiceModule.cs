﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;

namespace ChipBot.Core.Modules
{
    [RegisterToDevGuildOnly]
    [ChipInteractionPrecondition(typeof(RequireOwners))]
    internal class ServiceModule : ChipInteractionModuleBase
    {
        private static Dictionary<string, IBackgroundService> servicesList;
        private readonly ILogger logger;

        public ServiceModule(ILogger<ServiceModule> logger, IEnumerable<IBackgroundService> backgroundServices)
        {
            this.logger = logger;

            if (servicesList != null) return;

            servicesList = new Dictionary<string, IBackgroundService>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var service in backgroundServices)
            {
                servicesList.Add(service.GetType().Name, service);
            }
        }

        [SlashCommand("service", "Service commands")]
        public async Task DoServiceAction([Summary("name"), Autocomplete(typeof(ServiceNameAutoCompleter))] string service, ServiceAction action)
        {
            if (!servicesList.TryGetValue(service, out IBackgroundService instance)) { await ReplyFailAsync(); return; }

            switch (action)
            {
                case ServiceAction.restart:
                    if (instance.IsWorking) await instance.StopAsync();
                    await instance.StartAsync();
                    break;
                case ServiceAction.start:
                    if (!instance.IsWorking) await instance.StartAsync();
                    break;
                case ServiceAction.stop:
                    if (instance.IsWorking) await instance.StopAsync();
                    break;
                case ServiceAction.status:
                    await RespondAsync($"{service} {(instance.IsWorking ? "🟩" : "🟥")}");
                    return;
                default:
                    await ReplyFailAsync();
                    return;

            }

            await ReplySuccessAsync();
        }

        public enum ServiceAction
        {
            restart,
            start,
            stop,
            status
        }

        private class ServiceNameAutoCompleter : AutocompleteHandler
        {
            public override Task<AutocompletionResult> GenerateSuggestionsAsync(IInteractionContext context, IAutocompleteInteraction autocompleteInteraction, IParameterInfo parameter, IServiceProvider services)
            {
                var results = servicesList
                    .Where(service => service.Key.StartsWith(autocompleteInteraction.Data.Current.Value.ToString().Trim(), StringComparison.InvariantCultureIgnoreCase))
                    .Select(service => new AutocompleteResult(service.Key, service.Key));

                return Task.FromResult(AutocompletionResult.FromSuccess(results.Take(25)));
            }
        }
    }
}