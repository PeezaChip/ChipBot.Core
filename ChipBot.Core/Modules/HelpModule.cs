﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Extensions;
using ChipBot.Core.Implementations;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Services.Web;
using Discord;
using Discord.Interactions;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ChipBot.Core.Modules
{
    internal class HelpModule : ChipInteractionModuleBase
    {
        public InteractionService InteractionService { get; set; }
        public IBot Bot { get; set; }
        public SettingsService<GlobalConfig> GlobalConfig { get; set; }
        public TokenHandlerService TokenHandlerService { get; set; }
        public SettingsService<WebConfig> WebConfig { get; set; }

        private static string GetUptime() => (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString(@"dd\.hh\:mm\:ss");
        private static string GetHeapSize() => Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString();

        [SlashCommand("login", "Login to Chip Web")]
        [ChipInteractionPrecondition(typeof(RequireChannel))]
        public async Task WebLogin()
        {
            await RespondAsync(ephemeral: true, embed: new EmbedBuilder()
                .WithTitle("ChipWeb")
                .WithCurrentTimestamp().WithColor(Color.DarkBlue)
                .WithThumbnailUrl(Context.Client.CurrentUser.GetAvatarUrl(ImageFormat.Auto, 512))
                .WithDescription($"To login to ChipWeb please follow this " +
                $"[link]({new Uri(new Uri(WebConfig.Settings.Uri), $"/auth/login?token={TokenHandlerService.GenerateLoginTokenForUser(Context.User)}").AbsoluteUri})\n" +
                $"Link can be used only once and is only available for 5 minutes.").Build());
        }

        [RegisterToDevGuildOnly]
        [SlashCommand("info", "Displays information about bot")]
        public async Task InfoAsync()
        {
            var app = await Context.Client.GetApplicationInfoAsync();

            var embed = new EmbedBuilder().WithAuthor(Context.Client.GetUser(GlobalConfig.Settings.Owners.First()))
                .WithColor(Color.Gold).WithDescription($"{Context.Client.CurrentUser.Username} {Bot.Description}")
                .WithFooter("Created").WithTimestamp(DateTimeOffset.FromUnixTimeMilliseconds(Bot.CreatedTS))
                .WithThumbnailUrl(Bot.Thumbnail ?? Context.Client.CurrentUser.GetAvatarUrl(size: 256))
                .AddField("ℹ️ Info",
                    $"- Library: Discord.Net `({DiscordConfig.Version})`\n" +
                    $"- Runtime: {RuntimeInformation.FrameworkDescription} {RuntimeInformation.ProcessArchitecture}\n" +
                    $"`({RuntimeInformation.OSDescription} {RuntimeInformation.OSArchitecture})`\n" +
                    $"- Version: `{Bot.GetVersion()}`\n" +
                    $"- Core Version `{Assembly.GetExecutingAssembly().GetVersion()}`\n" +
                    $"- Uptime: `{GetUptime()}`")
                .AddField("📊 Stats",
                    $"- Heap Size: `{GetHeapSize()}MiB`\n" +
                    $"- Guilds: `{Context.Client.Guilds.Count}`\n" +
                    $"- Channels: `{Context.Client.Guilds.Sum(g => g.Channels.Count)}`\n" +
                    $"- Users: `{Context.Client.Guilds.Sum(g => g.Users.Count)}`\n" +
                    $"- Slash Commands: `{InteractionService.SlashCommands.Count}` in `{InteractionService.Modules.Count}` modules");

            await ReplyEmbedAsync(embed);
        }

        [RegisterToDevGuildOnly]
        [SlashCommand("ping", "Checks if bot is alive.")]
        public async Task Ping()
            => await RespondAsync($"Pong! `[{Context.Client.Latency} ms]`");

        [RegisterToDevGuildOnly]
        [SlashCommand("ip", "Gets IP address of a host.")]
        [ChipInteractionPrecondition(typeof(RequireOwners))]
        public async Task GetIp()
        {
            using var client = new HttpClient();
            var response = await client.GetStringAsync($"https://api.ipify.org?format=text");
            await RespondAsync($"`{response}`", ephemeral: true);
        }
    }
}