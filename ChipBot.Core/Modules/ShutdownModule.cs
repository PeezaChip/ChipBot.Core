﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Implementations;
using Discord.Interactions;

namespace ChipBot.Core.Modules
{
    [RegisterToDevGuildOnly]
    [ChipInteractionPrecondition(typeof(RequireOwners))]
    internal class ShutDownModule : ChipInteractionModuleBase
    {
        private Task ExitBot(int code)
        {
            Environment.Exit(code);
            return Task.CompletedTask;
        }

        [SlashCommand("bot", "Bot related actions")]
        public async Task ExitBot(BotAction action)
        {
            await RespondAsync(action.ToString());
            await ExitBot((int)action);
        }

        public enum BotAction
        {
            shutdown = 0,
            restart = 2
        }
    }
}