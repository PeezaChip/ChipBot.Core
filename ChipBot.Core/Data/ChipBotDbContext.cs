﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ChipBot.Core.Data
{
    /// <summary>
    /// Core Database context
    /// </summary>
    [ChipDbContext("ChipBot.Core")]
    public class ChipBotDbContext : DbContext
    {
        public DbSet<ChipSettings> Settings { get; set; }
        public DbSet<ChipUserSettings> UserSettings { get; set; }
        public DbSet<ChipKeyValue> Dictionary { get; set; }
        public DbSet<CommandExecutionModel> CommandExecutions { get; set; }
        public DbSet<NotifyInfo> Notifies { get; set; }
        public DbSet<ApiApplication> ApiApplications { get; set; }

        /// <summary>
        /// Initializes a new <see cref="ChipBotDbContext"/>
        /// </summary>
        /// <param name="options">options used to create db context</param>
        public ChipBotDbContext(DbContextOptions options) : base(options) { }
    }
}
