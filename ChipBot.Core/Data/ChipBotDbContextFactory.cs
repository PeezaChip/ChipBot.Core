﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace ChipBot.Core.Data
{
    public class ChipBotDbContextFactory : ChipBotDbContextFactory<ChipBotDbContext> { }

    /// <summary>
    /// Represents a base factory that is used to make DbContext
    /// </summary>
    /// <typeparam name="TContext">Type of DbContext</typeparam>
    public class ChipBotDbContextFactory<TContext> : IDesignTimeDbContextFactory<TContext>, IDbContextFactory<TContext>, IChipDbFactory where TContext : DbContext
    {
        /// <summary>
        /// Name of the environment variable that will be used to get database host. Default value is localhost
        /// </summary>
        private const string ENV_DB_HOST = "CHIP_DB_HOST";
        /// <summary>
        /// Name of the environment variable that will be used to get database port. Default value is 5432
        /// </summary>
        private const string ENV_DB_PORT = "CHIP_DB_PORT";

        /// <summary>
        /// Name of the environment variable that will be used to get database user. Default value is postgres
        /// </summary>
        private const string ENV_DB_USERNAME = "CHIP_DB_USERNAME";
        /// <summary>
        /// Name of the environment variable that will be used to get database password. Default value is null
        /// </summary>
        private const string ENV_DB_PASSWORD = "CHIP_DB_PASSWORD";

        /// <summary>
        /// Name of the environment variable that will be used to get database name. Default value is database
        /// </summary>
        private const string ENV_DB_NAME = "CHIP_DB_NAME";

        /// <summary>
        /// Name of the environment variable that will be used to auto apply migration. Default value is false
        /// </summary>
        private const string ENV_DB_AUTO_MIGRATE = "CHIP_DB_AUTO_MIGRATE";

        /// <summary>
        /// Name of the environment variable that will be used to turn on/off sensitive logging. Default value is false
        /// </summary>
        private const string ENV_DB_SENSITIVE_LOG_ENABLE = "CHIP_DB_SENSITIVE_LOG_ENABLE";

        private readonly string host = "localhost";
        private readonly string port = "5432";
        private readonly string username = "postgres";
        private readonly string pass = null;
        private readonly string dbname = "database";
        private readonly bool sensitiveLog = false;

        protected string migrationAssembly = "ChipBot";

        /// <summary>
        /// Initializes a new <see cref="CoreDbContextFactory{T}"/> class
        /// </summary>
        public ChipBotDbContextFactory()
        {
            var newHost = Environment.GetEnvironmentVariable(ENV_DB_HOST);
            var newPort = Environment.GetEnvironmentVariable(ENV_DB_PORT);
            var newUsername = Environment.GetEnvironmentVariable(ENV_DB_USERNAME);
            var newPassword = Environment.GetEnvironmentVariable(ENV_DB_PASSWORD);
            var newDbName = Environment.GetEnvironmentVariable(ENV_DB_NAME);
            _ = bool.TryParse(Environment.GetEnvironmentVariable(ENV_DB_SENSITIVE_LOG_ENABLE), out sensitiveLog);

            if (newHost != null) host = newHost;
            if (newPort != null) port = newPort;
            if (newUsername != null) username = newUsername;
            if (newPassword != null) pass = newPassword;
            if (newDbName != null) dbname = newDbName;

            GetMigrationAssembly();
        }

        public void GetMigrationAssembly()
        {
            var attr = typeof(TContext)
                .GetCustomAttribute<ChipDbContextAttribute>();
            if (attr == null) return;

            migrationAssembly = attr.Assembly;
        }

        public void ApplyMigrationIfNeeded()
        {
            _ = bool.TryParse(Environment.GetEnvironmentVariable(ENV_DB_AUTO_MIGRATE), out var autoMigrate);
            if (autoMigrate)
            {
                CreateDbContext().Database.Migrate();
            }
        }

        /// <summary>
        /// Creates a new instance of a derived context
        /// </summary>
        /// <param name="args">Arguments provided by the design-time service.</param>
        /// <returns>An instance of TContext</returns>
        public TContext CreateDbContext(string[] args)
            => CreateDbContext();

        /// <summary>
        /// Creates a new instance of a derived context
        /// </summary>
        /// <returns>An instance of TContext</returns>
        public TContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();
            optionsBuilder.EnableSensitiveDataLogging(sensitiveLog);
            optionsBuilder.UseNpgsql($"Server={host};Port={port};Database={dbname};UserID={username};Password={pass}", o =>
            {
                o.MigrationsAssembly(migrationAssembly);
                o.EnableRetryOnFailure(2, TimeSpan.FromSeconds(1), null);
                o.CommandTimeout(10);
                o.SetPostgresVersion(15, 4);
            });

            return (TContext)Activator.CreateInstance(typeof(TContext), optionsBuilder.Options);
        }
    }
}
