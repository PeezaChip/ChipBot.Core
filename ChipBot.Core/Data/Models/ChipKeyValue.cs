﻿using System.ComponentModel.DataAnnotations;

namespace ChipBot.Core.Data.Models
{
    /// <summary>
    /// Represents a key/value entity
    /// </summary>
    public class ChipKeyValue
    {
        /// <summary>
        /// Key
        /// </summary>
        [Key]
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [Required]
        public string Value { get; set; }
    }
}
