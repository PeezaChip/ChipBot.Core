﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChipBot.Core.Data.Models
{
    /// <summary>
    /// Represents a user config entity
    /// </summary>
    [PrimaryKey(nameof(Name), nameof(UserId))]
    public class ChipUserSettings
    {
        /// <summary>
        /// Configuration name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Discord User id
        /// </summary>
        public ulong UserId { get; set; }

        /// <summary>
        /// Configuration JSON
        /// </summary>
        [Column(TypeName = "jsonb")]
        [Required]
        public string Json { get; set; }
    }
}
