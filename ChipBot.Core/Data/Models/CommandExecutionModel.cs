﻿using Discord;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChipBot.Core.Data.Models
{
    [Index(nameof(FailedReason))]
    [PrimaryKey(nameof(Name), nameof(DateTime))]
    public class CommandExecutionModel
    {
        public string Name { get; set; }
        public DateTimeOffset DateTime { get; set; } = DateTimeOffset.UtcNow;

        public string Parameters { get; set; }

        public string FailedReason { get; set; }

        public ulong ExecutorId => User.Id;
        public string ExecutorFriendlyName => $"{User.Username}#{User.DiscriminatorValue}";

        [NotMapped]
        public IUser User { get; set; }
    }
}
