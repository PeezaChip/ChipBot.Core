﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChipBot.Core.Data.Models
{
    /// <summary>
    /// Represents a config entity
    /// </summary>
    public class ChipSettings
    {
        /// <summary>
        /// Configuration name
        /// </summary>
        [Key]
        public string Name { get; set; }

        /// <summary>
        /// Configuration JSON
        /// </summary>
        [Column(TypeName = "jsonb")]
        [Required]
        public string Json { get; set; }
    }
}
