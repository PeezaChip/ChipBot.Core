﻿using System.ComponentModel.DataAnnotations;

namespace ChipBot.Core.Data.Models
{
    public class NotifyInfo
    {
        [Key]
        public string ServiceName { get; set; }

        [Required]
        public DateTimeOffset Notified { get; set; }
    }
}
