﻿using Microsoft.EntityFrameworkCore;

namespace ChipBot.Core.Data.Models
{
    [PrimaryKey(nameof(Name), nameof(UserId))]
    public class ApiApplication
    {
        public ulong UserId { get; set; }

        public bool IsEnabled { get; set; }

        public string Name { get; set; }

        public string AppSecret { get; set; }

        public List<string> Scopes { get; set; }
    }
}
