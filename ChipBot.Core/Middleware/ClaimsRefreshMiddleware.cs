﻿using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Web;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace ChipBot.Core.Middleware
{
    public class ClaimsRefreshMiddleware
    {
        private readonly UserClaimsService userClaimsService;
        private readonly RequestDelegate next;

        public ClaimsRefreshMiddleware(UserClaimsService userClaimsService, RequestDelegate next)
        {
            this.userClaimsService = userClaimsService;
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var expiry = context.User.FindFirst(ClaimTypes.Expiration);

                if (expiry == null || new DateTimeOffset(long.Parse(expiry.Value), TimeSpan.Zero) < DateTimeOffset.UtcNow)
                {
                    await context.SignInAsync(AuthDefaults.Scheme, userClaimsService.GetPrincipal(context.GetCurrentDiscordUser()), new AuthenticationProperties());
                }
            }

            await next(context);
        }
    }
}
