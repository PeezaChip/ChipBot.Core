﻿using ChipBot.Core.Data;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace ChipBot.Core.Services.Web
{
    internal class IssuerKeyResolver
    {
        private readonly ChipBotDbContextFactory dbContextFactory;

        public IssuerKeyResolver(ChipBotDbContextFactory dbContextFactory)
            => this.dbContextFactory = dbContextFactory;

        public IEnumerable<SecurityKey> ResolveKey(string token, SecurityToken securityToken, string kid, TokenValidationParameters validationParameters)
        {
            if (securityToken is not JsonWebToken jwt) return empty;

            using var dbContext = dbContextFactory.CreateDbContext();
            var isuer = jwt.Issuer;
            if (!jwt.TryGetClaim(ClaimTypes.NameIdentifier, out var idClaim)) return empty;

            var userId = ulong.Parse(idClaim.Value);

            var app = dbContext.ApiApplications.FirstOrDefault(app => app.UserId == userId && app.Name == isuer);

            return app == null && app.IsEnabled
                ? empty
                : new List<SecurityKey>() { new SymmetricSecurityKey(Convert.FromBase64String(app.AppSecret)) };
        }

        private readonly IEnumerable<SecurityKey> empty = Array.Empty<SecurityKey>();
    }
}
