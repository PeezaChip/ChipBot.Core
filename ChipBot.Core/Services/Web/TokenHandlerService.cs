﻿using ChipBot.Core.Abstract;
using Discord;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Caching.Memory;
using System.Security.Cryptography;

namespace ChipBot.Core.Services.Web
{
    public class TokenHandlerService : ChipService<TokenHandlerService>
    {
        private readonly IMemoryCache cache;

        public TokenHandlerService(ILogger<TokenHandlerService> logger, IMemoryCache cache) : base(logger)
            => this.cache = cache;

        public string GenerateLoginTokenForUser(IUser user)
        {
            var token = GenerateLoginToken();
            var key = GetCacheKey(token);

            cache.Set(key, user, TimeSpan.FromMinutes(5));
            return token;
        }

        public IUser GetUserFromToken(string token)
        {
            var key = GetCacheKey(token);

            if (cache.TryGetValue<IUser>(key, out var user))
            {
                cache.Remove(key);
                return user;
            }
            else
            {
                return null;
            }
        }

        private static string GenerateLoginToken()
        {
            var bytes = RandomNumberGenerator.GetBytes(30);
            return WebEncoders.Base64UrlEncode(bytes);
        }

        private static string GetCacheKey(string token)
            => $"chipweb_login_token_{token}";
    }
}
