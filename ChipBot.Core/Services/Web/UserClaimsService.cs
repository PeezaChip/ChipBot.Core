﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Shared.Constants;
using Discord;
using Discord.WebSocket;
using System.Security.Claims;

namespace ChipBot.Core.Services.Web
{
    public class UserClaimsService : ChipService<UserClaimsService>
    {
        private readonly SettingsService<GlobalConfig> globalConfig;
        private readonly DiscordSocketClient discord;

        public UserClaimsService(ILogger<UserClaimsService> logger, SettingsService<GlobalConfig> globalConfig, DiscordSocketClient discord) : base(logger)
        {
            this.globalConfig = globalConfig;
            this.discord = discord;
        }

        public ClaimsPrincipal GetPrincipal(IUser user)
            => new(GetIdentity(user));

        private ClaimsIdentity GetIdentity(IUser user)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, user.Username),
                new(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.UInteger64),
                new(ClaimDefaults.Avatar, user.GetAvatarUrl(ImageFormat.WebP, 512)),
                new(ClaimTypes.Expiration, DateTimeOffset.UtcNow.AddHours(12).Ticks.ToString(), ClaimValueTypes.Integer64)
            };

            AddRoles(claims, user);
            return new ClaimsIdentity(claims, AuthDefaults.Scheme);
        }

        private void AddRoles(List<Claim> claims, IUser user)
        {
            if (globalConfig.Settings.Owners.Contains(user.Id)) claims.Add(new(ClaimTypes.Role, RoleDefaults.Owner));

            var guildUser = discord.GetGuild(globalConfig.Settings.MainGuildId).GetUser(user.Id);
            if (globalConfig.Settings.EliteRoleIds.Any(eliteRoleId => guildUser.Roles.Select(role => role.Id).Contains(eliteRoleId))) claims.Add(new(ClaimTypes.Role, RoleDefaults.Elite));
        }
    }
}
