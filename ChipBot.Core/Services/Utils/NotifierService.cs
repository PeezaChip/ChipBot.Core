﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using static ChipBot.Core.DependencyInjection.DiscordInjections;

namespace ChipBot.Core.Services.Utils
{
    internal class NotifierService : BackgroundService<NotifierService>
    {
        private readonly IServiceProvider serviceProvider;
        private readonly SettingsService<NotifyConfig> notifyConfig;
        private readonly ChipBotDbContextFactory dbContextFactory;

        private readonly IEnumerable<Type> notifyTypes;
        private IEnumerable<string> NotifyServices => notifyConfig.Settings.NotifyServices ?? new List<string>();

        private SortedDictionary<DateTimeOffset, List<Type>> NotifyTimes;

        public override bool IsWorking => Work != null && !Work.IsCompleted;
        private Task Work;
        private CancellationTokenSource CancelToken;

        public NotifierService(ILogger<NotifierService> logger, IServiceProvider serviceProvider, SettingsService<NotifyConfig> notifyConfig, ChipBotDbContextFactory dbContextFactory, NotifyPackage notifyPackage)
            : base(logger)
        {
            this.serviceProvider = serviceProvider;
            this.notifyConfig = notifyConfig;
            this.dbContextFactory = dbContextFactory;

            notifyTypes = notifyPackage.Types;
        }

        public override Task StartAsync()
        {
            CancelToken = new CancellationTokenSource();
            Work = CheckNotifyServices(CancelToken.Token);
            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            if (CancelToken != null)
            {
                CancelToken.Cancel();
                CancelToken = null;
            }
            Work = null;
            return Task.CompletedTask;
        }

        private async Task CheckNotifyServices(CancellationToken token)
        {
            if (NotifyTimes == null)
            {
                InitialCheck();
            }

            while (!token.IsCancellationRequested)
            {
                logger.LogInformation($"Checking if settings has changed");
                CheckIfSettingsChanged();

                var now = DateTimeOffset.UtcNow;
                if (NotifyTimes.Any() && NotifyTimes.First().Key <= now)
                {
                    var servicesToNotify = new List<Type>();
                    foreach (var time in NotifyTimes.Keys.ToList())
                    {
                        if (time <= now)
                        {
                            servicesToNotify.AddRange(NotifyTimes[time]);
                            NotifyTimes.Remove(time);
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (servicesToNotify.Any())
                    {
                        logger.LogInformation($"Should notify: {string.Join(", ", servicesToNotify.Select(t => t.Name))}");
                        foreach (var service in servicesToNotify)
                        {
                            await NotifyService(service);
                            var attribute = GetNotifyAttribute(service);
                            var nextNotify = GetNextNotifyTime(service, now.Add(attribute.RepeatInterval), true);
                            InsertNotifyService(nextNotify, service);
                        }
                    }
                }

                var delay = TimeSpan.FromMinutes(5);
                if (NotifyTimes.Any())
                {
                    var untilFirstNotify = NotifyTimes.First().Key - DateTimeOffset.UtcNow;
                    if (untilFirstNotify < delay)
                    {
                        delay = untilFirstNotify > TimeSpan.Zero ? untilFirstNotify : TimeSpan.FromSeconds(1);
                    }
                }

                try
                {
                    logger.LogTrace($"Sleeping for {delay}");
                    await Task.Delay(delay, token);
                }
                catch (TaskCanceledException)
                {
                    break;
                }
            }

            NotifyTimes = null;
        }

        private void InitialCheck()
        {
            NotifyTimes = new SortedDictionary<DateTimeOffset, List<Type>>();

            logger.LogInformation($"Checking notify services");
            foreach (var notify in notifyTypes)
            {
                if (!NotifyServices.Contains(notify.Name)) continue;

                var shouldNotifyAt = GetNextNotifyTime(notify);

                InsertNotifyService(shouldNotifyAt, notify);
            }
        }

        private void CheckIfSettingsChanged()
        {
            var registeredServices = NotifyTimes.SelectMany(t => t.Value).Select(t => t.Name);
            var servicesToAdd = NotifyServices.Except(registeredServices).ToList();
            var servicesToRemove = registeredServices.Except(NotifyServices).ToList();

            if (servicesToRemove.Any())
            {
                logger.LogInformation($"Removing services {string.Join(", ", servicesToRemove)} from notify list");
                foreach (var kv in new SortedDictionary<DateTimeOffset, List<Type>>(NotifyTimes))
                {
                    var type = kv.Value.FirstOrDefault(t => servicesToRemove.Contains(t.Name));
                    if (type != null)
                    {
                        servicesToRemove.Remove(type.Name);
                        NotifyTimes[kv.Key].Remove(type);
                        if (!NotifyTimes[kv.Key].Any())
                        {
                            NotifyTimes.Remove(kv.Key);
                        }
                    }
                }
            }

            if (servicesToAdd.Any())
            {
                logger.LogInformation($"Adding services {string.Join(", ", servicesToRemove)} to notify list");
                foreach (var service in servicesToAdd)
                {
                    var type = notifyTypes.FirstOrDefault(t => t.Name == service);
                    if (type == null) continue;
                    InsertNotifyService(GetNextNotifyTime(type), type);
                }
            }
        }

        private void InsertNotifyService(DateTimeOffset dateTime, Type service)
        {
            if (NotifyTimes.TryGetValue(dateTime, out var value))
            {
                value.Add(service);
            }
            else
            {
                NotifyTimes.Add(dateTime, new List<Type>() { service });
            }
        }

        private DateTimeOffset GetNextNotifyTime(Type notify, DateTimeOffset? nextNotify = null, bool saveToDatabase = false)
        {
            try
            {
                using var dbContext = dbContextFactory.CreateDbContext();
                logger.LogTrace($"Getting next notify time for {notify.Name}");
                if (!nextNotify.HasValue)
                {
                    logger.LogInformation($"Getting next notify time for {notify.Name} from database");
                    var notifyInfo = dbContext.Notifies.Find(notify.Name);
                    nextNotify = notifyInfo?.Notified ?? DateTimeOffset.MinValue;
                }
                var attribute = GetNotifyAttribute(notify);

                if (nextNotify.Value.Hour < attribute.NoEarlierThan)
                {
                    nextNotify = nextNotify.Value.AddHours(attribute.NoEarlierThan - nextNotify.Value.Hour - 1).AddMinutes(60 - nextNotify.Value.Minute);
                }

                if (saveToDatabase)
                {
                    logger.LogInformation($"Saving next notify time for {notify.Name} {nextNotify.Value}");
                    var notifyInfo = dbContext.Notifies.Find(notify.Name);
                    if (notifyInfo != null)
                    {
                        notifyInfo.Notified = nextNotify.Value;
                        dbContext.Notifies.Update(notifyInfo);
                    }
                    else
                    {
                        dbContext.Notifies.Add(new NotifyInfo() { Notified = nextNotify.Value, ServiceName = notify.Name });
                    }
                    dbContext.SaveChanges();
                }

                logger.LogTrace($"Next notify time for {notify.Name} is {nextNotify.Value}");
                return nextNotify.Value;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Couldn't get next notify time for {notify.Name}");
                return DateTimeOffset.MaxValue;
            }
        }

        private async Task NotifyService(Type notify)
        {
            try
            {
                logger.LogInformation($"Executing notify for {notify.Name}");
                var service = (INotifyService)serviceProvider.GetService(notify);
                await service.Notify(); // ToDo: implement timeout
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Notify failed for {notify.Name}: ");
            }
        }

        private static NotifyAttribute GetNotifyAttribute(Type service)
            => (NotifyAttribute)Attribute.GetCustomAttribute(service, typeof(NotifyAttribute)) ?? NotifyAttribute.Default;
    }
}
