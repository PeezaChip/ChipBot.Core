﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;

namespace ChipBot.Core.Services.Utils
{
    [Chipject]
    public class DictionaryService : ChipService<DictionaryService>
    {
        private readonly ChipBotDbContextFactory<ChipBotDbContext> dbContextFactory;

        public DictionaryService(ILogger<DictionaryService> log, ChipBotDbContextFactory<ChipBotDbContext> dbContextFactory) : base(log)
            => this.dbContextFactory = dbContextFactory;

        public void Add(string key, string value)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.Dictionary.Add(new ChipKeyValue { Key = key, Value = value });
            dbContext.SaveChanges();
        }

        public void Remove(string key)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var kv = dbContext.Dictionary.Find(key);

            if (kv != null)
            {
                dbContext.Dictionary.Remove(kv);
            }

            dbContext.SaveChanges();
        }

        public string Get(string key)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var kv = dbContext.Dictionary.Find(key);
            return kv?.Value;
        }

        public void AddOrUpdate(string key, string value)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var kv = dbContext.Dictionary.Find(key);

            if (kv != null)
            {
                kv.Value = value;
                dbContext.Dictionary.Update(kv);
            }
            else
            {
                dbContext.Dictionary.Add(new ChipKeyValue { Key = key, Value = value });
            }

            dbContext.SaveChanges();
        }
    }
}
