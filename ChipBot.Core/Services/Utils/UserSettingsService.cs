﻿using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using System.Text.Json;

namespace ChipBot.Core.Services.Utils
{
    public class UserSettingsService<TSettings> where TSettings : class, new()
    {
        private readonly IConfiguration configuration;
        private readonly ChipBotDbContextFactory dbContextFactory;

        public Dictionary<ulong, TSettings> UserSettings { get; private set; }

        public UserSettingsService(IConfiguration configuration, ChipBotDbContextFactory dbContextFactory)
        {
            this.configuration = configuration;
            this.dbContextFactory = dbContextFactory;

            LoadConfiguration();
            WatchChanges();
        }

        public TSettings Get(ulong userId)
            => UserSettings.TryGetValue(userId, out var value) ? value : new();

        public void SaveConfiguration(TSettings settings, ulong userId)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var current = dbContext.UserSettings.FirstOrDefault(s => s.Name == typeof(TSettings).Name && s.UserId == userId);
            if (current == null)
            {
                dbContext.Add(new ChipUserSettings()
                {
                    Name = typeof(TSettings).Name,
                    UserId = userId,
                    Json = JsonSerializer.Serialize(settings)
                });
            }
            else
            {
                current.Json = JsonSerializer.Serialize(settings);
                dbContext.Update(current);
            }

            dbContext.SaveChanges();

            if (UserSettings.ContainsKey(userId))
            {
                UserSettings[userId] = settings;
            }
            else
            {
                UserSettings.Add(userId, settings);
            }
        }

        public void LoadConfiguration()
        {
            var settings = new Dictionary<string, TSettings>();
            configuration.Bind(typeof(TSettings).Name, settings);
            UserSettings = settings.ToDictionary(kv => ulong.Parse(kv.Key), kv => kv.Value);
        }

        private void WatchChanges()
            => _ = configuration.GetReloadToken().RegisterChangeCallback(OnConfigurationChange, null);

        private void OnConfigurationChange(object state)
        {
            LoadConfiguration();
            WatchChanges();
        }
    }
}
