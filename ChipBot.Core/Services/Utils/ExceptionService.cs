﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Configuration.Models;
using Discord;
using Discord.WebSocket;

namespace ChipBot.Core.Services.Utils
{
    internal class ExceptionService : ChipService<ExceptionService>
    {
        public readonly DiscordSocketClient discord;
        public readonly SettingsService<ExceptionConfig> exceptionConfig;

        private ITextChannel ExceptionChannel { get; set; }

        public ExceptionService(DiscordSocketClient discord, ILogger<ExceptionService> logger, SettingsService<ExceptionConfig> exceptionConfig, StartupService discordEntryService) : base(logger)
        {
            this.discord = discord;
            this.exceptionConfig = exceptionConfig;

            discordEntryService.OnStartupReady += OnStartupReady;

            AppDomain.CurrentDomain.FirstChanceException += async (sender, EventArgs) =>
            {
                logger.LogCritical(EventArgs.Exception, "Caught FirstChanceException");

                if (ExceptionChannel != null)
                {
                    var ignoreFrom = exceptionConfig.Settings.IgnoreExceptionsFrom;
                    if (ignoreFrom != null && ignoreFrom.Contains(EventArgs.Exception.Source)) return;
                    if (discord.ConnectionState != ConnectionState.Connected) return;

                    try
                    {
                        var embed = new EmbedBuilder().WithCurrentTimestamp()
                            .WithDescription($"Got an **'{EventArgs.Exception.Message}'** from **'{EventArgs.Exception.Source}'** when trying to do **'{EventArgs.Exception.TargetSite.Name}()'**.");
                        embed.AddField("Stack", $"```cs\n{(EventArgs.Exception.StackTrace.Length < 1000 ? EventArgs.Exception.StackTrace : EventArgs.Exception.StackTrace[..1000] + "...")}```");

                        await ExceptionChannel.SendMessageAsync("", embed: embed.Build());
                    }
                    catch { }
                }
            };
        }

        private Task OnStartupReady()
        {
            ExceptionChannel = (ITextChannel)discord.GetChannel(exceptionConfig.Settings.ExceptionChannelId);
            return Task.CompletedTask;
        }
    }
}
