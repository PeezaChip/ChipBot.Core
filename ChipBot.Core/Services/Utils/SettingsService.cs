﻿using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using System.Text.Json;

namespace ChipBot.Core.Services.Utils
{
    public class SettingsService<TSettings> where TSettings : class, new()
    {
        private readonly IConfiguration configuration;
        private readonly ChipBotDbContextFactory dbContextFactory;

        public TSettings Settings { get; private set; }

        public SettingsService(IConfiguration configuration, ChipBotDbContextFactory dbContextFactory)
        {
            this.configuration = configuration;
            this.dbContextFactory = dbContextFactory;

            LoadConfiguration();
            WatchChanges();
        }

        public void SaveConfiguration(TSettings settings)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.Settings.Update(new ChipSettings()
            {
                Name = typeof(TSettings).Name,
                Json = JsonSerializer.Serialize(settings)
            });
            dbContext.SaveChanges();

            Settings = settings;
        }

        public void LoadConfiguration()
        {
            Settings = new TSettings();
            configuration.Bind(typeof(TSettings).Name, Settings);
        }

        private void WatchChanges()
            => _ = configuration.GetReloadToken().RegisterChangeCallback(OnConfigurationChange, null);

        private void OnConfigurationChange(object state)
        {
            LoadConfiguration();
            WatchChanges();
        }
    }
}
