﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using System.Net;

namespace ChipBot.Core.Services.Utils
{
    [Chipject]
    public class ProxyService : ChipService<ProxyService>
    {
        private readonly IWebProxy proxy = null;

        public ProxyService(ILogger<ProxyService> logger, SettingsService<NetworkConfig> networkConfig) : base(logger)
            => proxy = GetProxy(networkConfig.Settings);

        public IWebProxy GetProxy(NetworkConfig settings = null)
        {
            if (settings == null) return proxy;

            if (string.IsNullOrEmpty(settings.Host) || string.IsNullOrEmpty(settings.Proto)) return null;

            var wp = new WebProxy($"{settings.Proto}://{settings.Host}:{settings.Port}");
            if (!string.IsNullOrEmpty(settings.Username) && !string.IsNullOrEmpty(settings.Password))
            {
                wp.Credentials = new NetworkCredential(settings.Username, settings.Password);
            }
            return wp;
        }
    }
}
