﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.WebSocket;
using static ChipBot.Core.DependencyInjection.DiscordInjections;

namespace ChipBot.Core.Services
{
    internal class StartupService : ChipService<StartupService>
    {
        private readonly DiscordSocketClient discord;
        private readonly IServiceProvider serviceProvider;
        private readonly IBot bot;
        private readonly SettingsService<StartupConfig> startupConfig;
        private readonly SettingsService<GlobalConfig> globalConfig;

        private DateTime downTime;

        public event Func<Task> OnStartupReady;

        private ITextChannel StatusChannel { get; set; }

        public StartupService(ILogger<StartupService> logger, DiscordSocketClient discord, IServiceProvider serviceProvider, IBot bot,
            SettingsService<StartupConfig> startupConfig, SettingsService<GlobalConfig> globalConfig) : base(logger)
        {
            this.discord = discord;
            this.serviceProvider = serviceProvider;
            this.bot = bot;
            this.startupConfig = startupConfig;
            this.globalConfig = globalConfig;

            discord.Log += DiscordLog;
        }

        public async Task StartAsync()
        {
            discord.Ready += DiscordReady;
            try
            {
                logger.LogInformation($"Logging into discord");
                await discord.LoginAsync(TokenType.Bot, startupConfig.Settings.DiscordToken);
                logger.LogInformation($"Starting discord");
                await discord.StartAsync();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, $"Couldn't start discord");
            }
        }

        private async Task DiscordReady()
        {
            discord.Ready -= DiscordReady;

            StatusChannel = (ITextChannel)discord.GetChannel(startupConfig.Settings.StatusChannelId);

            discord.Connected += DiscordConnected;
            discord.Disconnected += DiscordDisconnected;
            logger.LogInformation($"Notifying that the bot has started");
            await StatusChannel.SendMessageAsync($"`{DateTime.Now}`\nBot is ready and running.\nVersion `{bot.GetType().Assembly.GetVersion()}`\nCore Version `{GetType().Assembly.GetVersion()}`\nDebug mode `{(globalConfig.Settings.IsDebug ? "On" : "Off")}`");

            logger.LogInformation($"Requiring services");
            RequireServices();

            logger.LogInformation($"Invoking OnStartupReady");
            OnStartupReady?.Invoke();

            logger.LogInformation($"Starting background services");
            _ = StartServicesOnReady();
        }

        private void RequireServices()
        {
            if (startupConfig.Settings.RequireServices == null) return;

            var services = serviceProvider.GetRequiredService<ChipPackage>();

            foreach (var serviceName in startupConfig.Settings.RequireServices)
            {
                try
                {
                    var serviceType = services.Types.FirstOrDefault(t => t.Name == serviceName);
                    logger.LogInformation($"Requiring '{serviceName}'");
                    _ = serviceProvider.GetRequiredService(serviceType);
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Couldn't require '{serviceName}'");
                }
            }
        }

        private async Task StartServicesOnReady()
        {
            if (startupConfig.Settings.StartOnReady == null) return;

            var services = serviceProvider.GetRequiredService<BackgroundPackage>();
            foreach (var service in services.Types)
            {
                var serviceName = service.Name;
                try
                {
                    if (!startupConfig.Settings.StartOnReady.Contains(serviceName)) continue;
                    var serviceInstance = serviceProvider.GetRequiredService(service) as IBackgroundService;
                    logger.LogInformation($"Trying to start '{serviceName}'");
                    await StartService(serviceInstance);
                    logger.LogInformation($"Started '{serviceName}'");
                }
                catch (Exception e)
                {
                    logger.LogError(e, $"Couldn't start '{serviceName}'");
                }
            }
        }

        private static async Task StartService(IBackgroundService service)
        {
            var startTask = service.StartAsync();
            if (await Task.WhenAny(startTask, Task.Delay(1000 * 5)) != startTask)
            {
                throw new Exception("Timeout");
            }
            else if (startTask.IsFaulted)
            {
                throw startTask.Exception;
            }
        }

        private Task DiscordDisconnected(Exception arg)
        {
            downTime = DateTime.Now;
            return Task.CompletedTask;
        }

        private async Task DiscordConnected()
        {
            var time = DateTime.Now - downTime;

            if (time.TotalMinutes > startupConfig.Settings.SupressDowntimeLessThanMinutes)
            {
                await StatusChannel.SendMessageAsync($"`{DateTime.Now}`\nReconnected to Discord\nDown time `{time.TotalMinutes:0.00}m`");
            }
        }

        private Task DiscordLog(LogMessage log)
        {
            logger.LogDiscordMessage(log);
            return Task.CompletedTask;
        }
    }
}
