﻿using ChipBot.Core.Abstract;
using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using ChipBot.Core.Extensions;
using ChipBot.Core.Implementations;
using ChipBot.Core.Services.Utils;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Newtonsoft.Json;
using System.Reflection;
using static ChipBot.Core.DependencyInjection.DiscordInjections;
using IResult = Discord.Interactions.IResult;

namespace ChipBot.Core.Services.Discord
{
    internal class InteractionHandlingService : BackgroundService<InteractionHandlingService>
    {
        private readonly DiscordSocketClient discord;
        private readonly InteractionService interaction;
        private readonly SettingsService<GlobalConfig> globalConfig;
        private readonly SettingsService<InteractionHandlingServiceConfig> interactionConfig;
        private readonly IServiceProvider serviceProvider;
        private readonly ChipBotDbContextFactory dbContextFactory;

        private bool RestrictGuild => globalConfig.Settings.IsDebug && globalConfig.Settings.RestrictGuilds;
        private bool RestrictUsers => globalConfig.Settings.IsDebug && globalConfig.Settings.RestrictUsers;
        private ulong DevGuild => globalConfig.Settings.DevGuildId;
        private IEnumerable<ulong> Owners => globalConfig.Settings.Owners;

        private bool _isWorking = false;
        public override bool IsWorking => _isWorking;

        /// <summary>
        /// Initializes a new <see cref="InteractionHandlingService"/> class
        /// </summary>
        /// <param name="discord"></param>
        /// <param name="interaction"></param>
        /// <param name="globalConfig"></param>
        /// <param name="logger"></param>
        public InteractionHandlingService(ILogger<InteractionHandlingService> logger, DiscordSocketClient discord, InteractionService interaction,
            SettingsService<GlobalConfig> globalConfig, SettingsService<InteractionHandlingServiceConfig> interactionConfig,
            IServiceProvider serviceProvider, ChipBotDbContextFactory dbContextFactory) : base(logger)
        {
            this.discord = discord;
            this.interaction = interaction;
            this.globalConfig = globalConfig;
            this.interactionConfig = interactionConfig;
            this.serviceProvider = serviceProvider;
            this.dbContextFactory = dbContextFactory;

            interaction.Log += InteractionLog;
            interaction.SlashCommandExecuted += SlashCommandExecuted;
        }

        public async override Task StartAsync()
        {
            _isWorking = true;
            discord.InteractionCreated += OnInteractionCreated;

            await PrepareCommands();
        }

        public override Task StopAsync()
        {
            _isWorking = false;
            discord.InteractionCreated -= OnInteractionCreated;
            return Task.CompletedTask;
        }

        private bool prepareCommands = false;
        private async Task PrepareCommands()
        {
            if (prepareCommands) return;
            prepareCommands = true;

            logger.LogInformation($"Loading type readers");
            LoadTypeReaders();
            logger.LogInformation($"Loading modules");
            var createdModules = await LoadModules();

            if (interactionConfig.Settings.AutoRegisterCommands)
            {
                logger.LogInformation($"AutoRegisterCommands is true");
                await RegisterCommands(createdModules);
            }
            else if (globalConfig.Settings.IsDebug)
            {
                logger.LogInformation($"Waiting for ##REGISTER_CMDS to register commands");
                async Task registerByCommand(SocketMessage rawMessage)
                {
                    if (rawMessage is not SocketUserMessage message) return;
                    if (message.Source != MessageSource.User) return;
                    if (!Owners.Contains(rawMessage.Author.Id)) return;
                    if (rawMessage.Type != MessageType.Default) return;

                    if (!"##REGISTER_CMDS".Equals(rawMessage.Content)) return;
                    await RegisterCommands(createdModules);

                    discord.MessageReceived -= registerByCommand;
                };

                discord.MessageReceived += registerByCommand;
            }
        }

        private Task InteractionLog(LogMessage log)
        {
            logger.LogDiscordMessage(log);
            return Task.CompletedTask;
        }

        private void LoadTypeReaders()
        {
            var typeConverters = serviceProvider.GetRequiredService<IEnumerable<TypeConverter>>();
            foreach (var typeConverter in typeConverters)
            {
                var attribute = typeConverter.GetType().GetCustomAttribute<ChipTypeReaderAttribute>();
                interaction.AddTypeConverter(attribute.Type, typeConverter);
                logger.LogInformation($"Loaded TypeConverter for {attribute.Type.Name}");
            }
        }

        private async Task<Dictionary<string, Type>> LoadModules()
        {
            var package = serviceProvider.GetRequiredService<ModulesPackage>();
            var created = new Dictionary<string, Type>();
            foreach (var m in package.Types)
            {
                if (m.IsNested) continue;
                logger.LogInformation($"Loading module {m.Name}");
                try
                {
                    await interaction.AddModuleAsync(m, serviceProvider);
                    created.Add(m.Name, m);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Can't create module {m.Name}");
                }
            }
            return created;
        }

        private async Task RegisterCommands(Dictionary<string, Type> createdModules)
        {
            try
            {
                logger.LogInformation($"Registering commands");
                var devCommands = new List<SlashCommandInfo>();
                var mainCommands = new List<SlashCommandInfo>();

                var devModules = new List<ModuleInfo>();
                var mainModules = new List<ModuleInfo>();

                foreach (var moduleInfo in interaction.Modules)
                {
                    if (!createdModules.TryGetValue(moduleInfo.Name, out var module)) continue;

                    if (moduleInfo.IsSlashGroup)
                    {
                        if (Attribute.GetCustomAttribute(module, typeof(RegisterToDevGuildOnly)) != null)
                        {
                            devModules.Add(moduleInfo);
                        }
                        else
                        {
                            mainModules.Add(moduleInfo);
                        }
                    }
                    else
                    {
                        if (Attribute.GetCustomAttribute(module, typeof(RegisterToDevGuildOnly)) != null)
                        {
                            devCommands.AddRange(moduleInfo.SlashCommands);
                        }
                        else
                        {
                            foreach (var command in moduleInfo.SlashCommands)
                            {
                                if (command.Attributes.Any(a => a is RegisterToDevGuildOnly))
                                {
                                    devCommands.Add(command);
                                }
                                else
                                {
                                    mainCommands.Add(command);
                                }
                            }
                        }
                    }
                }

                if (!globalConfig.Settings.IsDebug)
                {
                    await interaction.AddCommandsToGuildAsync(discord.GetGuild(globalConfig.Settings.MainGuildId), true, mainCommands.ToArray());
                    await interaction.AddModulesToGuildAsync(discord.GetGuild(globalConfig.Settings.MainGuildId), false, mainModules.ToArray());

                    foreach (var secondaryGuildId in globalConfig.Settings.SecondaryGuildIds)
                    {
                        await interaction.AddCommandsToGuildAsync(discord.GetGuild(secondaryGuildId), true, mainCommands.ToArray());
                        await interaction.AddModulesToGuildAsync(discord.GetGuild(secondaryGuildId), false, mainModules.ToArray());
                    }
                }

                await interaction.AddCommandsToGuildAsync(discord.GetGuild(globalConfig.Settings.DevGuildId), true, devCommands.Concat(mainCommands).ToArray());
                await interaction.AddModulesToGuildAsync(discord.GetGuild(globalConfig.Settings.DevGuildId), false, devModules.Concat(mainModules).ToArray());
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error while registering interactions");
            }
        }

        private Task OnInteractionCreated(SocketInteraction interaction)
        {
            var context = new ChipInteractionContext(discord, interaction);

            if (RestrictGuild && context.Guild.Id != DevGuild) return interaction.RespondAsync("Bot is running in development mode. And this guild is not allowed.", ephemeral: true);
            if (RestrictUsers && !Owners.Contains(context.User.Id)) return interaction.RespondAsync("Bot is running in development mode. And only bot owners allowed to invoke commands.", ephemeral: true);

            _ = ExecuteInteraction(context);
            return Task.CompletedTask;
        }

        private async Task<IResult> ExecuteInteraction(ChipInteractionContext context)
        {
            using var state = context.Channel.EnterTypingState();
            return await interaction.ExecuteCommandAsync(context, serviceProvider);
        }

        private Task SlashCommandExecuted(SlashCommandInfo command, IInteractionContext context, IResult result) => SlashCommandExecuted(command, (ChipInteractionContext)context, result);
        private async Task SlashCommandExecuted(SlashCommandInfo command, ChipInteractionContext context, IResult result)
        {
            var commandExecution = new CommandExecutionModel
            {
                Name = command.ToDBKey(), User = context.User, Parameters = context.Interaction.Data switch
                {
                    IApplicationCommandInteractionData data => JsonConvert.SerializeObject(data),
                    _ => context.Interaction.Data.ToString(),
                }
            };

            if (!result.IsSuccess)
            {
                var response = result.Error switch
                {
                    InteractionCommandError.UnmetPrecondition => $"Unmet Precondition: {result.ErrorReason}",
                    InteractionCommandError.UnknownCommand => "Unknown command",
                    InteractionCommandError.BadArgs => "Invalid number or arguments",
                    InteractionCommandError.Exception => $"Command exception:{result.ErrorReason}",
                    InteractionCommandError.Unsuccessful => "Command could not be executed",
                    _ => "Unknown error",
                };
                await context.Interaction.RespondAsync(response, ephemeral: true);
                commandExecution.FailedReason = response;

                foreach (var interceptor in context.InterceptorContext.Keys)
                {
                    await interceptor.OnCommandError(context, command.ToDBKey(), ChipExecutionResult.FromResult(result));
                }
            }

            using var dbContext = dbContextFactory.CreateDbContext();
            dbContext.CommandExecutions.Add(commandExecution);
            dbContext.SaveChanges();
        }
    }
}
