﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Extensions;
using ChipBot.Core.Policies.Requirements;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;

namespace ChipBot.Core.Policies
{
    [Chipject(ChipjectType.Singleton, typeof(IAuthorizationHandler))]
    public class ValidScopesHandler : AuthorizationHandler<ValidScopesRequirement>
    {
        private readonly ChipBotDbContextFactory dbContextFactory;

        public ValidScopesHandler(ChipBotDbContextFactory dbContextFactory)
            => this.dbContextFactory = dbContextFactory;

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ValidScopesRequirement requirement)
        {
            if (context.User.Identity.AuthenticationType == AuthDefaults.Scheme)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            try
            {
                if (context.Resource is HttpContext httpContext)
                {
                    using var dbContext = dbContextFactory.CreateDbContext();
                    var userId = httpContext.GetCurrentDiscordUserId();
                    var appIdClaim = httpContext.User.Claims.First(c => c.Type == JwtRegisteredClaimNames.Iss);
                    if (appIdClaim == null)
                    {
                        context.Fail(new AuthorizationFailureReason(this, "Incorrect app id"));
                        return Task.CompletedTask;
                    }

                    var app = dbContext.ApiApplications.FirstOrDefault(app => app.UserId == userId && app.Name == appIdClaim.Value);

                    if (app == null || !app.IsEnabled)
                    {
                        context.Fail(new AuthorizationFailureReason(this, "Missing app or it is disabled"));
                        return Task.CompletedTask;
                    }

                    var endpoint = httpContext.GetEndpoint();
                    var actionDescriptor = endpoint.Metadata.GetMetadata<ControllerActionDescriptor>();
                    var attr = actionDescriptor.ControllerTypeInfo.GetCustomAttribute<ChipApiAttribute>();

                    var requiredScope = attr.GroupName;

                    if (httpContext.Request.Method == HttpMethod.Get.Method)
                    {
                        requiredScope += ".read";
                    }
                    else
                    {
                        requiredScope += ".write";
                    }

                    if (app.Scopes?.Contains(requiredScope) ?? false)
                    {
                        context.Succeed(requirement);
                    }
                    else
                    {
                        context.Fail(new AuthorizationFailureReason(this, "Incorrect app scopes"));
                    }
                }
            }
            catch (Exception ex)
            {
                context.Fail(new AuthorizationFailureReason(this, ex.Message));
            }

            return Task.CompletedTask;
        }
    }
}
