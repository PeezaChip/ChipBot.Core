﻿using Microsoft.AspNetCore.Authorization;

namespace ChipBot.Core.Policies.Requirements
{
    public class ValidScopesRequirement : IAuthorizationRequirement { }
}
