﻿using ChipBot.Core.Services.Web;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;

namespace ChipBot.Core.DependencyInjection
{
    internal class ConfigureJwtBearerOptions : IConfigureNamedOptions<JwtBearerOptions>
    {
        private readonly IssuerKeyResolver issuerKeyResolver;

        public ConfigureJwtBearerOptions(IssuerKeyResolver issuerKeyResolver)
            => this.issuerKeyResolver = issuerKeyResolver;

        public void Configure(JwtBearerOptions options)
        {
            options.TokenValidationParameters.IssuerSigningKeyResolver = issuerKeyResolver.ResolveKey;

            options.TokenValidationParameters.ValidateIssuer = false;
            options.TokenValidationParameters.ValidateIssuerSigningKey = true;

            options.TokenValidationParameters.ValidateAudience = false;
            options.TokenValidationParameters.RequireExpirationTime = false;

            options.SaveToken = true;
        }

        public void Configure(string name, JwtBearerOptions options)
            => Configure(options);
    }
}
