﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Implementations;
using ChipBot.Core.Modules;
using ChipBot.Core.Services;
using ChipBot.Core.Services.Discord;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Services.Web;
using Discord.Interactions;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace ChipBot.Core.DependencyInjection
{
    internal static class DiscordInjections
    {
        public static IServiceCollection AddTypeReaders(this IServiceCollection services, IEnumerable<Type> types)
        {
            var typeConverterType = typeof(TypeConverter);
            foreach (var type in types.Where(t => t.IsSubclassOf(typeConverterType) && Attribute.IsDefined(t, typeof(ChipTypeReaderAttribute))))
            {
                services.AddSingleton(typeConverterType, type);
            }
            return services;
        }

        // change to keyed when fixed
        public class ModulesPackage { public List<Type> Types { get; set; } }
        public static IServiceCollection AddInteractionModules(this IServiceCollection services, IEnumerable<Type> types)
        {
            var moduleType = typeof(ChipInteractionModuleBase);
            var modules = types.Where(t => moduleType.IsAssignableFrom(t) && !t.IsAbstract).ToList();

            modules.Add(typeof(HelpModule));
            modules.Add(typeof(ShutDownModule));
            modules.Add(typeof(ServiceModule));

            var package = new ModulesPackage()
            {
                Types = modules
            };
            services.AddSingleton(package);

            return services;
        }

        // change to keyed when fixed
        public class ContextPackage { public List<Type> Types { get; set; } }
        public static IServiceCollection AddDbContexts(this IServiceCollection services, IEnumerable<Type> types)
        {
            var contextType = typeof(DbContext);
            var contexts = types
                .Where(t => contextType.IsAssignableFrom(t) && t.GetCustomAttribute<ChipDbContextAttribute>() != null)
                .ToList();

            var package = new ContextPackage()
            {
                Types = contexts
            };
            services.AddSingleton(package);

            return services;
        }

        // change to keyed when fixed
        public class ChipPackage { public List<Type> Types { get; set; } }
        public class NotifyPackage { public List<Type> Types { get; set; } }
        public class BackgroundPackage { public List<Type> Types { get; set; } }
        public static IServiceCollection AddServices(this IServiceCollection services, IEnumerable<Type> types)
        {
            var notifyService = typeof(INotifyService);
            var backgroundService = typeof(IBackgroundService);

            var typesToInject = types.Where(t => !t.IsAbstract && t.GetCustomAttribute<ChipjectAttribute>() != null);

            var chipPackage = new ChipPackage() { Types = new() };
            var notifyPackage = new NotifyPackage() { Types = new() };
            var backgroundPackage = new BackgroundPackage() { Types = new() };

            foreach (var type in typesToInject)
            {
                var attr = type.GetCustomAttribute<ChipjectAttribute>();
                switch (attr.Type)
                {
                    case ChipjectType.Singleton:
                        if (attr.InjectAs != null)
                        {
                            services.AddSingleton(attr.InjectAs, type);
                        }
                        else
                        {
                            services.AddSingleton(type);
                        }
                        break;

                    case ChipjectType.Scope:
                        if (attr.InjectAs != null)
                        {
                            services.AddScoped(attr.InjectAs, type);
                        }
                        else
                        {
                            services.AddScoped(type);
                        }
                        break;

                    case ChipjectType.Transient:
                        if (attr.InjectAs != null)
                        {
                            services.AddTransient(attr.InjectAs, type);
                        }
                        else
                        {
                            services.AddTransient(type);
                        }
                        break;
                }

                if (notifyService.IsAssignableFrom(type))
                {
                    notifyPackage.Types.Add(type);
                }
                else if (backgroundService.IsAssignableFrom(type))
                {
                    backgroundPackage.Types.Add(type);
                }
                else
                {
                    chipPackage.Types.Add(type);
                }
            }

            chipPackage.Types.Add(typeof(ExceptionService));
            services.AddSingleton<ExceptionService>();

            chipPackage.Types.Add(typeof(TokenHandlerService));
            services.AddSingleton<TokenHandlerService>();

            chipPackage.Types.Add(typeof(UserClaimsService));
            services.AddSingleton<UserClaimsService>();

            backgroundPackage.Types.Add(typeof(InteractionHandlingService));
            services.AddSingleton<InteractionHandlingService>();

            backgroundPackage.Types.Add(typeof(NotifierService));
            services.AddSingleton<NotifierService>();

            services.AddSingleton<StartupService>();
            services.AddSingleton<IssuerKeyResolver>();

#if DEBUG
            services.AddSingleton(types);
#endif

            services.AddSingleton(chipPackage);
            services.AddSingleton(notifyPackage);
            services.AddSingleton(backgroundPackage);
            return services;
        }

        public static IServiceCollection AddPlugins(this IServiceCollection services, IConfiguration configuration, IEnumerable<Type> types)
        {
            var pluginType = typeof(IPlugin);
            var plugins = types
                .Where(t => pluginType.IsAssignableFrom(t) && !t.IsAbstract)
                .ToList();

            foreach (var type in plugins)
            {
                var plugin = (IPlugin)Activator.CreateInstance(type);
                plugin.ModifyServiceCollection(configuration, services);
            }

            return services;
        }
    }
}
