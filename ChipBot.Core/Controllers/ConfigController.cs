﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using ChipBot.Core.Shared.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace ChipBot.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Roles = RoleDefaults.Owner)]
    [Produces("application/json")]
    [ChipApi(GroupName = "system", Version = "v1", Title = "ChipBot System V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ConfigController : ControllerBase
    {
        private readonly ChipBotDbContextFactory dbContextFactory;

        public ConfigController(ChipBotDbContextFactory dbContextFactory)
            => this.dbContextFactory = dbContextFactory;

        [HttpGet("list")]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult GetList()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            return Ok(dbContext.Settings.Select(s => s.Name).ToList());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult Get(string id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var json = dbContext.Settings.Find(id)?.Json;
            return json == null ? NotFound() : Ok(JsonSerializer.Deserialize<JsonDocument>(json));
        }

        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult Post(string id, [FromBody] string value)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var config = dbContext.Settings.Find(id);
            if (config == null)
            {
                config = new ChipSettings() { Name = id, Json = value };
                dbContext.Settings.Add(config);
            }
            else
            {
                config.Name = id;
                config.Json = value;
                dbContext.Settings.Update(config);
            }
            dbContext.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult Delete(string id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var config = dbContext.Settings.Find(id);
            if (config == null) return NotFound();
            dbContext.Settings.Remove(config);
            dbContext.SaveChanges();
            return Ok();
        }
    }
}
