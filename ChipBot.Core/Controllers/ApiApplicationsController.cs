﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Data;
using ChipBot.Core.Data.Models;
using ChipBot.Core.Extensions;
using ChipBot.Core.Shared.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;

namespace ChipBot.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [Produces("application/json")]
    [ChipApi(GroupName = "apiapps", Version = "v1", Title = "ChipBot Api Applications V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ApiApplicationsController : ControllerBase
    {
        private readonly ChipBotDbContextFactory dbContextFactory;
        private readonly List<ChipApiAttribute> chipApis;

        public ApiApplicationsController(ChipBotDbContextFactory dbContextFactory, List<ChipApiAttribute> chipApis)
        {
            this.dbContextFactory = dbContextFactory;
            this.chipApis = chipApis;
        }

        [HttpGet("list")]
        [ProducesResponseType(typeof(IEnumerable<ApiApplicationExpandedModel>), StatusCodes.Status200OK)]
        public IActionResult GetList()
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var userId = HttpContext.GetCurrentDiscordUserId();
            return Ok(dbContext.ApiApplications.Where(app => app.UserId == userId).Select(app => app.ToApiApplication()).ToList());
        }

        [HttpGet("scopes")]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        public IActionResult GetScopes()
            => Ok(chipApis.Select(api => api.GroupName));

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ApiApplicationExpandedModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(string id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var userId = HttpContext.GetCurrentDiscordUserId();
            var app = dbContext.ApiApplications.FirstOrDefault(app => app.Name == id && app.UserId == userId);
            if (app == null) return NotFound();

            var result = app.ToApiExpandedApplication();

            var key = new SymmetricSecurityKey(Convert.FromBase64String(app.AppSecret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
            var token = new JwtSecurityToken(app.Name, claims: HttpContext.User.Claims, signingCredentials: creds, expires: null);

            result.Key = new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(result);
        }

        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Post(string id, ApiApplicationModel app)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var userId = HttpContext.GetCurrentDiscordUserId();
            var existing = dbContext.ApiApplications.FirstOrDefault(app => app.Name == id && app.UserId == userId);
            if (existing == null)
            {
                dbContext.ApiApplications.Add(new ApiApplication()
                {
                    IsEnabled = app.IsEnabled,
                    Name = app.Name,
                    Scopes = app.Scopes,
                    UserId = userId,
                    AppSecret = GenerateSecret()
                });
            }
            else
            {
                existing.IsEnabled = app.IsEnabled;
                existing.Scopes = app.Scopes;
                dbContext.ApiApplications.Update(existing);
            }
            dbContext.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(string id)
        {
            using var dbContext = dbContextFactory.CreateDbContext();
            var userId = HttpContext.GetCurrentDiscordUserId();
            var app = dbContext.ApiApplications.FirstOrDefault(app => app.Name == id && app.UserId == userId);
            if (app == null) return NotFound();
            dbContext.ApiApplications.Remove(app);
            dbContext.SaveChanges();
            return Ok();
        }

        private static string GenerateSecret()
        {
            var bytes = RandomNumberGenerator.GetBytes(128);
            return Convert.ToBase64String(bytes);
        }
    }
}
