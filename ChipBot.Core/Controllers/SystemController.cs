﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Attributes;
using ChipBot.Core.Constants;
using ChipBot.Core.Extensions;
using ChipBot.Core.Shared.Api;
using ChipBot.Core.Shared.Constants;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ChipBot.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ChipApi(GroupName = "system", Version = "v1", Title = "ChipBot System V1")]
    public class SystemController : ControllerBase
    {
        private readonly ILogger<SystemController> logger;
        private readonly IBot bot;
        private readonly DiscordSocketClient discord;
        private readonly InteractionService interactionService;

        public SystemController(ILogger<SystemController> logger, IBot bot, DiscordSocketClient discord, InteractionService interactionService)
        {
            this.logger = logger;
            this.bot = bot;
            this.discord = discord;
            this.interactionService = interactionService;
        }

        [HttpGet("health")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult GetHealth()
            => Ok(true);

        [HttpGet("info")]
        [Authorize(Roles = RoleDefaults.Owner)]
        [ProducesResponseType(typeof(SystemInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult GetInfo()
        {
            var process = Process.GetCurrentProcess();

            var info = new SystemInfo()
            {
                CPU = RuntimeInformation.ProcessArchitecture.ToString(),
                OS = $"{RuntimeInformation.OSDescription} {RuntimeInformation.OSArchitecture}",
                ServerTime = DateTime.Now,
                ProcessStartTime = process.StartTime,
                MachineName = process.MachineName,
                Memory = Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString(),

                Runtime = RuntimeInformation.FrameworkDescription,
                DiscordNetVersion = DiscordConfig.Version,
                CoreVersion = Assembly.GetExecutingAssembly().GetVersion(),
                BotVersion = bot.GetVersion(),

                GuildCount = discord.Guilds.Count,
                Channels = discord.Guilds.Sum(g => g.Channels.Count),
                Users = discord.Guilds.Sum(g => g.Users.Count),
                Commands = interactionService.SlashCommands.Count
            };

            return Ok(info);
        }

        [HttpGet("services")]
        [Authorize(Roles = RoleDefaults.Owner)]
        [ProducesResponseType(typeof(Dictionary<string, bool>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public IActionResult GetServices()
        {
            var serviceTypes = HttpContext.RequestServices.GetRequiredService<DependencyInjection.DiscordInjections.BackgroundPackage>();
            var services = new Dictionary<string, bool>();
            foreach (var serviceType in serviceTypes.Types)
            {
                if (HttpContext.RequestServices.GetService(serviceType) is not IBackgroundService service) continue;
                services.Add(serviceType.Name, service.IsWorking);
            }
            return Ok(services);
        }

        [HttpPut("services")]
        [Authorize(Roles = RoleDefaults.Owner)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public async Task<IActionResult> PutServices(Dictionary<string, ServiceApiAction> serviceActions)
        {
            var serviceTypes = HttpContext.RequestServices.GetRequiredService<DependencyInjection.DiscordInjections.BackgroundPackage>();
            foreach (var (serviceName, action) in serviceActions)
            {
                var serviceType = serviceTypes.Types.FirstOrDefault(s => s.Name == serviceName);
                if (serviceType == null) continue;

                try
                {
                    if (HttpContext.RequestServices.GetService(serviceType) is not IBackgroundService service) throw new Exception("not a background service");
                    switch (action)
                    {
                        case ServiceApiAction.restart:
                            if (service.IsWorking) await service.StopAsync();
                            await service.StartAsync();
                            break;
                        case ServiceApiAction.start:
                            if (!service.IsWorking) await service.StartAsync();
                            break;
                        case ServiceApiAction.stop:
                            if (service.IsWorking) await service.StopAsync();
                            break;
                    }

                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"Error while performing {action} on {serviceName}");
                }
            }
            return Ok();
        }
    }
}
