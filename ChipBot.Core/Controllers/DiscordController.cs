﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Constants;
using ChipBot.Core.Extensions;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Shared.Api;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ChipBot.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [Authorize]
    [ChipApi(GroupName = "discord", Version = "v1", Title = "ChipBot Discord V1")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class DiscordController : ControllerBase
    {
        private readonly DiscordSocketClient discord;
        private readonly SettingsService<GlobalConfig> config;

        public DiscordController(DiscordSocketClient discord, SettingsService<GlobalConfig> config)
        {
            this.discord = discord;
            this.config = config;
        }

        [HttpGet("users")]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        [ProducesResponseType(typeof(IEnumerable<ApiDiscordUser>), StatusCodes.Status200OK)]
        public IActionResult GetUsers()
        {
            var guild = discord.GetGuild(config.Settings.MainGuildId);
            return Ok(guild.Users.Select(u => u.ToApiDiscordUser()));
        }

        [HttpGet("roles")]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        [ProducesResponseType(typeof(IEnumerable<ApiDiscordRole>), StatusCodes.Status200OK)]
        public IActionResult GetRoles()
        {
            var guild = discord.GetGuild(config.Settings.MainGuildId);
            return Ok(guild.Roles.Select(r => r.ToApiDiscordRole()));
        }
    }
}
