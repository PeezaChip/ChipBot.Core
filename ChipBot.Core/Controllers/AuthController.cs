﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Constants;
using ChipBot.Core.Services.Utils;
using ChipBot.Core.Services.Web;
using ChipBot.Core.Shared.Api;
using ChipBot.Core.Shared.Constants;
using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ChipBot.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [ChipApi(GroupName = "auth", Version = "v1", Title = "ChipBot Auth V1")]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> logger;
        private readonly TokenHandlerService tokenService;
        private readonly UserClaimsService userClaimsService;

        public AuthController(ILogger<AuthController> logger, TokenHandlerService tokenService, UserClaimsService userClaimsService)
        {
            this.logger = logger;
            this.tokenService = tokenService;
            this.userClaimsService = userClaimsService;
        }

        [HttpGet("login")]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public async Task<IActionResult> Login([FromQuery] string token)
        {
            logger.LogInformation($"Got login request with token {token}");
            if (string.IsNullOrWhiteSpace(token)) return Unauthorized();
            var user = tokenService.GetUserFromToken(token);
            if (user == null) return Unauthorized();

            await HttpContext.SignInAsync(AuthDefaults.Scheme, userClaimsService.GetPrincipal(user), new AuthenticationProperties());
            return Redirect("/");
        }

        [HttpGet("logout")]
        [ProducesResponseType(StatusCodes.Status302Found)]
        [ResponseCache(CacheProfileName = CachingConstants.NoCache)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(AuthDefaults.Scheme);
            return Redirect("/");
        }

        [HttpGet("userinfo")]
        [ProducesResponseType(typeof(UserInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize]
        [ResponseCache(CacheProfileName = CachingConstants.MinuteUserCache)]
        public IActionResult UserInfo()
        {
            return Ok(new UserInfo()
            {
                Username = HttpContext.User.FindFirstValue(ClaimTypes.Name),
                UserId = ulong.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier)),
                Avatar = HttpContext.User.FindFirstValue(ClaimDefaults.Avatar),
                Roles = HttpContext.User.FindAll(ClaimTypes.Role).Select(claim => claim.Value).ToArray()
            });
        }
    }
}
