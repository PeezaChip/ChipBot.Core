﻿namespace ChipBot.Core.Attributes
{
    /// <summary>
    /// Represents an attribute that will be used to automate <see cref="Discord.Commands.TypeReader"/>s loading. <seealso cref="Services.Discord.InteractionHandlingService.LoadTypeReaders"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ChipTypeReaderAttribute : Attribute
    {
        /// <summary>
        /// Output type of a type reader
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// Constructor for <see cref="ChipTypeReaderAttribute"/>
        /// </summary>
        /// <param name="type">Output type of a type reader</param>
        public ChipTypeReaderAttribute(Type type)
        {
            Type = type;
        }
    }
}
