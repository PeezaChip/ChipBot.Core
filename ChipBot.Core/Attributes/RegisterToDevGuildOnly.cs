﻿using Discord.Interactions;

namespace ChipBot.Core.Attributes
{
    /// <summary>
    /// <inheritdoc />
    /// But will be registered to <see cref="Configuration.Models.GlobalConfig.DevGuildId"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RegisterToDevGuildOnly : DontAutoRegisterAttribute { }
}
