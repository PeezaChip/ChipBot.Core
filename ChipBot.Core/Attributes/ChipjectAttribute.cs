﻿namespace ChipBot.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ChipjectAttribute : Attribute
    {
        public ChipjectType Type { get; }
        public Type InjectAs { get; }

        public ChipjectAttribute(ChipjectType type = ChipjectType.Singleton, Type injectAs = null)
        {
            Type = type;
            InjectAs = injectAs;
        }
    }

    public enum ChipjectType
    {
        Singleton,
        Scope,
        Transient
    }
}
