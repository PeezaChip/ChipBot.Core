﻿namespace ChipBot.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NotifyAttribute : Attribute
    {
        public static NotifyAttribute Default { get; } = new();

        public double RepeatIntervalSeconds { get; set; } = TimeSpan.FromHours(23).Add(TimeSpan.FromMinutes(30)).TotalSeconds;
        public TimeSpan RepeatInterval => TimeSpan.FromSeconds(RepeatIntervalSeconds);
        public int NoEarlierThan { get; set; } = 12;
    }
}
