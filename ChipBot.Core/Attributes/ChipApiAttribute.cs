﻿using Microsoft.AspNetCore.Mvc;

namespace ChipBot.Core.Attributes
{
    public class ChipApiAttribute : ApiExplorerSettingsAttribute
    {
        public string Title { get; set; }
        public string Version { get; set; }

        public string SwaggerLink => $"/swagger/{GroupName}/swagger.json";
        public string GroupKey => $"{GroupName}_{Version}";
    }
}
