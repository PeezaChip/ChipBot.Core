﻿namespace ChipBot.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ChipDbContextAttribute : Attribute
    {
        public string Assembly { get; }

        public ChipDbContextAttribute(string assembly)
            => Assembly = assembly;
    }
}
