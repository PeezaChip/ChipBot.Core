﻿namespace ChipBot.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ChipHubAttribute : Attribute
    {
        public string Path { get; }

        public ChipHubAttribute(string path) => Path = path;
    }
}
