﻿using Discord.Interactions;

namespace ChipBot.Core.Core.Preconditions
{
    public class ChipPreconditionResult
    {
        public string ErrorReason { get; private set; }
        public bool IsSuccess { get; private set; }

        public static ChipPreconditionResult FromError(string error) => new() { IsSuccess = false, ErrorReason = error };
        public static ChipPreconditionResult FromSuccess() => new() { IsSuccess = true, ErrorReason = null };

        public PreconditionResult ToInteractionResult() => IsSuccess ? PreconditionResult.FromSuccess() : PreconditionResult.FromError(ErrorReason);
    }
}
