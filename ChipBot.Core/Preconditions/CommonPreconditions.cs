﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Configuration.Models;
using ChipBot.Core.Extensions;
using ChipBot.Core.Interceptors;
using ChipBot.Core.Services.Utils;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace ChipBot.Core.Core.Preconditions
{
    public class InterceptorPrecondition : IChipPrecondition
    {
        private static List<IInterceptor> interceptors;

        public async Task<ChipPreconditionResult> CheckPermissions(IChipContext context, IServiceProvider services, object command)
        {
            if (interceptors == null)
            {
                interceptors = services.GetServices(typeof(IInterceptor)).Cast<IInterceptor>().ToList();
            }

            foreach (var interceptor in interceptors)
            {
                if (!interceptor.ShouldIntercept(context, command)) continue;
                var dbKey = command is ICommandInfo interInfo ? interInfo.ToDBKey() : null;
                var result = await interceptor.Intercept(context, dbKey);
                if (result.IsIntercepted)
                {
                    return ChipPreconditionResult.FromError(result.Reason);
                }
            }

            return ChipPreconditionResult.FromSuccess();
        }
    }

    public class RequireOwners : IChipPrecondition
    {
        public async Task<ChipPreconditionResult> CheckPermissions(IChipContext context, IServiceProvider services, object command)
        {
            var config = services.GetRequiredService<SettingsService<GlobalConfig>>().Settings;
            return config.Owners.Contains(context.User.Id)
                ? await Task.FromResult(ChipPreconditionResult.FromSuccess())
                : await Task.FromResult(ChipPreconditionResult.FromError("You must be the owner of the guild or bot to run this command."));
        }
    }

    public class RequireElite : IChipPrecondition
    {
        private static readonly Task<ChipPreconditionResult> NotUser = Task.FromResult(ChipPreconditionResult.FromError("This command may only be ran in a guild."));
        private static readonly Task<ChipPreconditionResult> NotElevated = Task.FromResult(ChipPreconditionResult.FromError("You are not elite member of this guild."));
        private static readonly Task<ChipPreconditionResult> Elevated = Task.FromResult(ChipPreconditionResult.FromSuccess());

        public async Task<ChipPreconditionResult> CheckPermissions(IChipContext context, IServiceProvider services, object command)
        {
            var config = services.GetRequiredService<SettingsService<GlobalConfig>>().Settings;

            return context.User is not SocketGuildUser user
                ? await NotUser
                : config.EliteRoleIds
                .Any(x => user.Roles.Select(y => y.Id).Contains(x))
                ? await Elevated
                : await NotElevated;
        }
    }

    public class RequireChannel : IChipPrecondition
    {
        private static readonly string NotAllowed = "This channel is not allowed.";

        public Task<ChipPreconditionResult> CheckPermissions(IChipContext context, IServiceProvider services, object command)
        {
            var config = services.GetRequiredService<SettingsService<GlobalConfig>>().Settings;

            return context.Channel is SocketDMChannel || config.AllowedChannelIds.Contains(context.Channel.Id)
                ? Task.FromResult(ChipPreconditionResult.FromSuccess())
                : Task.FromResult(ChipPreconditionResult.FromError(NotAllowed));
        }
    }
}
