﻿using ChipBot.Core.Abstract.Interfaces;

namespace ChipBot.Core.Core.Preconditions
{
    public interface IChipPrecondition
    {
        Task<ChipPreconditionResult> CheckPermissions(IChipContext context, IServiceProvider services, object command);
    }
}
