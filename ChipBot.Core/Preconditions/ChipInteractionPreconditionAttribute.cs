﻿using ChipBot.Core.Abstract.Interfaces;
using Discord;
using Discord.Interactions;

namespace ChipBot.Core.Core.Preconditions
{
    public class ChipInteractionPreconditionAttribute : PreconditionAttribute
    {
        private readonly IChipPrecondition precondition = null;

        public override async Task<PreconditionResult> CheckRequirementsAsync(IInteractionContext context, ICommandInfo commandInfo, IServiceProvider services)
            => (await precondition.CheckPermissions((IChipContext)context, services, commandInfo)).ToInteractionResult();

        public ChipInteractionPreconditionAttribute(Type preconditionType, params object[] args)
        {
            precondition = (IChipPrecondition)Activator.CreateInstance(preconditionType, args);
        }
    }
}
